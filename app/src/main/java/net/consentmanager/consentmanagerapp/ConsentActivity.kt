package net.consentmanager.consentmanagerapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import net.consentmanager.cmpsdk.CmpManager
import net.consentmanager.consentmanagerapp.databinding.ActivityConsentBinding
import net.consentmanager.consentmanagerapp.databinding.ActivityMainBinding

class ConsentActivity : AppCompatActivity() {
    private lateinit var binding: ActivityConsentBinding

    companion object {
        const val DEFAULT_CMP_ID:Int = 201
        const val CMP_DOMAIN:String = "www.consentmanager.net"
        const val CMP_APP_NAME:String = "Example"
        const val LANG:String = "DE"
        const val TAG:String = "CMP_EXAMPLE_APP"
    }

    private val consentTool: CmpManager by lazy {
        CmpManager.create(
            intent.getStringExtra("CMP_ID").toString(),
            CMP_DOMAIN,
            CMP_APP_NAME,
            "DE"
        )
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityConsentBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        initClickListener()
    }

    fun consentWillBeShown(): Boolean {
        return true
    }

    private fun initClickListener() {

        binding.buttonCalledLast.setOnClickListener {
            val calledLast = consentTool.getLastRequest()
            //val calledLast = consentTool.getCalledLast(applicationContext)
            Log.d(TAG, "Called Last $calledLast")
        }

        //consentTool.setOpenCmpConsentToolViewListener(this, findViewById(R.id.button_show_cmp))
        // cmp manager add buttton TODO
        binding.buttonNeedAcceptance.setOnClickListener {
            val res = consentTool.needUserConsent().toString()
            //val res = consentTool.needsAcceptance(applicationContext).toString()
            Log.d(TAG, "Need Acceptance: $res")
            Toast.makeText(applicationContext, res, Toast.LENGTH_LONG).show()
        }

        binding.buttonExportCmp.setOnClickListener {
            val res = consentTool.exportConsentString()
            //CMPConsentTool.getStatus(applicationContext)
            Log.d(TAG, "Export CMP: $res")
            Toast.makeText(applicationContext, res, Toast.LENGTH_LONG).show()
        }

        binding.buttonResetCmp.setOnClickListener {
            Log.d(TAG, "Clear all Values")
            consentTool.reset()
        }
        binding.buttonShowPurposes.setOnClickListener {
            val res = consentTool.getPurposes().toString()
            //val res = consentTool.getPurposes(applicationContext)
            Log.d(TAG, "Purposes: $res")
            Toast.makeText(applicationContext, res, Toast.LENGTH_LONG).show()
        }
        binding.buttonShowVendors.setOnClickListener {
            val res = consentTool.getVendors().toString()
            // val res = consentTool.getVendorsString(applicationContext)
            Log.d(TAG, "VendorList: $res")
            Toast.makeText(applicationContext, res, Toast.LENGTH_LONG).show()
        }

        binding.submitConsentVendor.setOnClickListener {
            val isIab: Boolean = binding.checkBoxIsIabVendor.isChecked
            val vendorId: String = binding.editTextVendorVendor.text.toString()
            val res = consentTool.hasVendor(vendorId).toString()
            //val res =  consentTool.hasVendorConsent(applicationContext, vendorId.toString(), isIab).toString()
            Log.d(TAG, "hasVendorConsent: $res")
            Toast.makeText(applicationContext, res, Toast.LENGTH_SHORT).show()
        }
    }
}