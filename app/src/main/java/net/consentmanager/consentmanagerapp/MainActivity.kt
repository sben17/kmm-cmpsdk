package net.consentmanager.consentmanagerapp

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import net.consentmanager.cmpsdk.CmpConfig
import net.consentmanager.cmpsdk.CmpManager
import net.consentmanager.cmpsdk.eventListener.CmpEventListener
import net.consentmanager.consentmanagerapp.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private val defaultId: String = "201"
    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)


        binding.submitConsentInit.setOnClickListener {
            submitConsentInit()
        }
    }

    private fun submitConsentInit() {
        var id = binding.editTextCmpId.text.toString()
        if (id.isEmpty()) {
            id = defaultId;
        }
        val intent = Intent(applicationContext, ConsentActivity::class.java).apply {
            putExtra("CMP_ID", id)
        }
        // TODO for import Consent and Ad id
//        if(consentString.isNotEmpty()) {
//            intent.putExtra("consent", consentString)
//        }
        startActivity(intent)

    }
}