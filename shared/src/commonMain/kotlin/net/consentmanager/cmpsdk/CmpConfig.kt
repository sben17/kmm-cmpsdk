package net.consentmanager.cmpsdk

import android.content.Context
import java.io.UnsupportedEncodingException
import java.net.URLEncoder
import kotlin.jvm.JvmStatic


private const val log = false

private const val DEBUG = true

@Suppress("VARIABLE_IN_SINGLETON_WITHOUT_THREAD_LOCAL")
class CmpConfig private constructor(
    id: String,
    domain: String,
    appName: String,
    language: String,
) {
    private var id: String? = null
    private var domain: String? = null
    private var appName: String? = null
    private var language: String? = null
    private var timeout: Int? = null
    private var idfa: String? = null

    private fun setLanguage(language: String) {
        this.language = language
    }

    private fun setAppName(app_name: String) {
        appName = encodeValue(app_name)
    }

    private fun setServerDomain(server_domain: String) {
        domain = server_domain
    }

    fun setIdfa(idfa: String?): CmpConfig {
        this.idfa = idfa
        return this
    }

    fun getIdfa(): String {
        return idfa ?: ""
    }

    fun getTimeout(): Int {
        return if (timeout == null) DEFAULT_TIMEOUT else timeout!!
    }

    fun setTimeout(timeout: Int) {
        this.timeout = timeout
    }

    override fun toString(): String {
        return "CmpConfig{" +
                "id=" + id +
                ", idfa='" + idfa + '\'' +
                ", serverDomain='" + domain + '\'' +
                ", appName='" + appName + '\'' +
                ", language='" + language + '\'' +
                ", timeout=" + timeout +
                '}'
    }

    fun getLanguage(): String? {
        return language
    }

    fun getAppName(): String? {
        return appName
    }

    fun getId(): String? {
        return id
    }

    fun getServerDomain(): String? {
        return domain
    }

    companion object {
        private const val DEFAULT_TIMEOUT = 5000
        private var instance: CmpConfig? = null

        @JvmStatic
        fun create(
            id: String,
            domain: String,
            appName: String,
            language: String
        ): CmpConfig? {
            if (instance == null) {
                instance = CmpConfig(id, domain, appName, language)
            } else {
                instance!!.id = id
                instance!!.setServerDomain(domain)
                instance!!.setAppName(appName)
                instance!!.setLanguage(language)
            }
            return instance
        }

        fun getInstance(context: Context?): CmpConfig? {
            if (instance != null) {
                return instance
            }
            throw Exception("Cmp Config are not set yet")
        }

        private fun encodeValue(value: String): String {
            return try {
                URLEncoder.encode(value, "UTF-8")
            } catch (e: UnsupportedEncodingException) {
                throw UnsupportedEncodingException(
                    "The Server domain URL given is not valid to UTF-8 encode: $value",
                )
            }
        }
    }
}