package net.consentmanager.cmpsdk.model

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
class CmpUserConsent(
    @SerialName("cmpId")
    private val cmpId: String,
    @SerialName("addtlConsent")
    val addTlConsent: String,
    @SerialName("consentstring")
    val consentString: String,
    @SerialName("gdprApplies")
    val gdprApplies: Boolean,
    @SerialName("googleVendorConsents")
    val googleVendorList: HashMap<String, Boolean>,
    @SerialName("hasGlobalScope")
    val hasGlobalScope: Boolean,
    @SerialName("publisherCC")
    val publisherCC: String,
    @SerialName("purposeLI")
    private val purposeMap: HashMap<String, Boolean>,
    @SerialName("regulation")
    val regulation: Int,
    @SerialName("regulationKey")
    val regulationKey: String,
    @SerialName("tcfcompliant")
    val tcfCompliant: Boolean,
    @SerialName("tcfversion")
    val tcfVersion: Int,
    @SerialName("uspstring")
    val usPrivacyString: String,
    @SerialName("vendorsList")
    private val allVendorsList: List<Vendor>,
    @SerialName("vendorConsents")
    private val vendorConsents: HashMap<String, Boolean>
) {

    fun hasPurpose(id: String): Boolean {
        return purposeMap.containsKey(id)
    }

    fun hasVendor(id: String): Boolean {
        return vendorConsents.containsKey(id.lowercase());
    }

    fun getCmpId(): String {
        return cmpId
    }


    fun isValid(): Boolean {
        //TODO add possible validation restrictions
        return consentString.isNotEmpty()
    }

    fun getPurposeList(): List<String> {
        return purposeMap.keys.toList()
    }

    fun getAllVendorList(): List<String> {
        return allVendorsList.map { item -> (item.id) }
    }

    fun getVendorList(): List<String> {
        return vendorConsents.keys.toList()
    }

    companion object {
        fun createEmpty(): CmpUserConsent {
            return CmpUserConsent(
                "",
                "",
                "",
                false,
                HashMap<String, Boolean>(),
                false,
                "",
                HashMap<String, Boolean>(),
                0,
                "",
                false,
                0,
                "",
                emptyList(),
                HashMap<String, Boolean>(),
            )
        }
    }

}