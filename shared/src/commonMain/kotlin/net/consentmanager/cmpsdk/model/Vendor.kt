package net.consentmanager.cmpsdk.model

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
class Vendor(
    @SerialName("id")
    val id: String,
    @SerialName("iabid")
    val iabId: String,
    @SerialName("systemid")
    val systemId: String,
    @SerialName("googleid")
    val googleId: String,
    @SerialName("name")
    val name: String
)