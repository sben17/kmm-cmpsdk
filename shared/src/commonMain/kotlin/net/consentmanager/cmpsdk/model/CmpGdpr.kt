package net.consentmanager.cmpsdk.model

enum class CmpGdpr(val subject: String) {
    GDPR_UNKNOWN("-1"),
    GDPR_DISABLED("0"),
    GDPR_ENABLED("1");

    companion object {
        private val map = CmpGdpr.values().associateBy(CmpGdpr::subject)
        fun fromString(s: String) = map[s]
    }
}