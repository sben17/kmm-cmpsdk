package net.consentmanager.cmpsdk.model

import kotlinx.datetime.Clock
import kotlinx.datetime.Instant
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
class CmpState(
    @SerialName("state")
    val state: CmpStateName,
    @SerialName("layerState")
    val layerState: CmpLayer,
    @SerialName("created")
    val timeStamp: Instant,
    @SerialName("message")
    val message: String
    ) {
    companion object {
        fun initial(): CmpState {
            return CmpState(
                CmpStateName.INITIAL,
                CmpLayer.LAYER_CLOSED,
                Clock.System.now(),
                ""
            )
        }

        fun error(layerState: CmpLayer, message: String): CmpState {
            return CmpState(
                CmpStateName.ERROR,
                layerState,
                Clock.System.now(),
                message
            )
        }

        fun consentSaved(): CmpState {
            return CmpState(
                CmpStateName.HAS_CONSENT,
                CmpLayer.LAYER_CLOSED,
                Clock.System.now(),
                ""
            )
        }
    }

    override fun toString(): String {
        return "CmpState(state=$state, layerState=$layerState, timeStamp=$timeStamp, message='$message')"
    }
}

@Serializable
enum class CmpStateName {
    @SerialName("initial")
    INITIAL,
    @SerialName("needConsent")
    NEED_CONSENT,
    @SerialName("hasConsent")
    HAS_CONSENT,
    @SerialName("error")
    ERROR
}

@Serializable
enum class CmpLayer {
    @SerialName("open")
    LAYER_OPEN,
    @SerialName("closed")
    LAYER_CLOSED
}