package net.consentmanager.cmpsdk.model

enum class CmpRegulation(val value: Int) {
    CCPA_APPLIES(2),
    GDPR_APPLIES(1),
    UNKNOWN(0);

    companion object {
        private val map = values().associateBy(CmpRegulation::value)
        fun fromInt(type: Int) = map[type]
    }
}