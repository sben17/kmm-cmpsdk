package net.consentmanager.cmpsdk.eventListener

import android.util.Log
import io.github.aakira.napier.Napier

private const val TAG = "CMP:Event"

interface CmpEventListener {
    fun consentLayerOpened() {
        Napier.i("Consent Layer opened", tag = TAG)
    }

    fun consentLayerNotOpened() {
        Napier.i("Consent Layer not opened", tag = TAG)
    }

    fun consentLayerClosed() {
        Napier.i("Consent Layer opened", tag = TAG)
    }

    fun userConsentSaved(consent: String) {
        Napier.i("Consent saved: $consent", tag = TAG)
    }

    fun userConsentImported(consent: String) {
        Napier.i("Consent imported: $consent", tag = TAG)
    }

    fun errorOccurred(message: String) {
        Napier.i(message, tag = TAG)
    }

    fun networkErrorOccurred(message: String) {
        Napier.i(message, tag = TAG)
    }
}