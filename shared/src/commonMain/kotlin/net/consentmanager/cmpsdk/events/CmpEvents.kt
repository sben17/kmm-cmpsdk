package net.consentmanager.cmpsdk.events

interface CmpEvents {
    fun initialEvent()
    fun needConsentEvent()
    fun hasConsentEvent()
    fun errorOnCloseEvent()
}