package net.consentmanager.cmpsdk

import com.russhwolf.settings.Settings
import net.consentmanager.cmpsdk.infrastructure.CmpStatusRepository
import net.consentmanager.cmpsdk.infrastructure.CmpStatusRepositoryInterface
import net.consentmanager.cmpsdk.infrastructure.CmpUserConsentRepository
import net.consentmanager.cmpsdk.infrastructure.CmpUserConsentRepositoryInterface
import net.consentmanager.cmpsdk.service.CmpUserConsentService
import org.kodein.di.*


val serviceDi = DI {
    bind<Settings> { singleton { Settings() } }
    bind<CmpStatusRepositoryInterface> { singleton { CmpStatusRepository(instance()) }}
    bind<CmpUserConsentRepositoryInterface> { singleton { CmpUserConsentRepository(instance()) }}
    bind<CmpUserConsentService> { singleton { CmpUserConsentService(instance(), instance())}}
}