package net.consentmanager.cmpsdk.service

import android.util.Log
import io.github.aakira.napier.Napier
import net.consentmanager.cmpsdk.infrastructure.CmpStatusRepositoryInterface
import net.consentmanager.cmpsdk.infrastructure.CmpUserConsentRepositoryInterface
import net.consentmanager.cmpsdk.model.*
import net.consentmanager.cmpsdk.utils.isValidPurpose
import net.consentmanager.cmpsdk.utils.isValidVendor

private const val TAG: String = "CMP:UserConsent"

class CmpUserConsentService(private val cmpUserConsentRepository: CmpUserConsentRepositoryInterface, private val cmpStatusRepository: CmpStatusRepositoryInterface) {

    fun hasVendor(vendorId: String): Boolean {
        if (!isValidVendor(vendorId)) {
            throw IllegalArgumentException("The vendor Id '$vendorId' is not a valid Vendor")
        }
        val cmpUserConsent: CmpUserConsent? = cmpUserConsentRepository.getUserConsent()

        if (cmpUserConsent == null) {
            Napier.e("Error, no user consent given", tag = TAG)
            return true
        }

        val regulation: CmpRegulation? = CmpRegulation.fromInt(cmpUserConsent.regulation)
        // Unknown RegulationStatus always need to return true
        if (regulation == CmpRegulation.UNKNOWN) {
            return true
        }
        return cmpUserConsent.hasVendor(vendorId)
    }

    fun hasPurpose(purposeId: String): Boolean {
        if (!isValidPurpose(purposeId)) {
            throw IllegalArgumentException("The purpose Id '%$purposeId' is not a valid Purpose")
        }

        val cmpUserConsent: CmpUserConsent? = cmpUserConsentRepository.getUserConsent()

        if (cmpUserConsent == null) {
            Napier.e("Error, no user consent given", tag = TAG)
            return true
        }

        val regulation: CmpRegulation? = CmpRegulation.fromInt(cmpUserConsent.regulation)

        // Unknown RegulationStatus always need to return true
        if (regulation == CmpRegulation.UNKNOWN) {
            return true
        }

        return cmpUserConsent.hasPurpose(purposeId)
    }

    fun resetUserConsent() {
        cmpUserConsentRepository.removeUserConsent()
        cmpStatusRepository.saveState(CmpState.initial())
    }

    fun getCmpId(): String {
        return cmpUserConsentRepository.getUserConsent()?.getCmpId() ?: ""
    }

    fun getUserConsentString(): String {
        val userConsent: CmpUserConsent? = cmpUserConsentRepository.getUserConsent()
        if (userConsent == null) {
            Napier.e("Error, no user consent given", tag = TAG)
            return ""
        }
        return userConsent.consentString
    }

    fun getVendors(): List<String> {
        //TODO
        return emptyList()
    }

    fun hasPurposeForVendor(purposeId: String, vendorId: String): Boolean {
        val cmpUserConsent: CmpUserConsent? = cmpUserConsentRepository.getUserConsent()
        // TODO check for vendor in purpose ?
        return false
    }

    fun getPurposeList(): List<String> {
        val userConsent: CmpUserConsent? = cmpUserConsentRepository.getUserConsent()
        if (userConsent == null) {
            Napier.e("Error, no user consent given", tag = TAG)
            return emptyList()
        }
        return userConsent.getPurposeList()
    }

    /**
     * checks on given rules if the user need to give consent
     */
    fun needUserConsent(): Boolean {
        val cmpState: CmpState? = cmpStatusRepository.getState()
        Napier.d(cmpState.toString(), tag=TAG)
        if (cmpState != null && cmpState.state == (CmpStateName.HAS_CONSENT) ) {
            return false
        }
        return true
    }

    /**
     * import Consent String Data
     */
    fun importConsentString(consent: String): CmpState {
        // TODO request data object and save consent to repository
        val jsonData : String = consent // TODO change
        val cmpState : CmpState
        if(jsonData.isNotEmpty()) {
            cmpState =  CmpState.error(
                CmpLayer.LAYER_CLOSED,
                "Can't get the consent data from import"
            )
            cmpStatusRepository.saveState(
                cmpState
            )
            Log.e(TAG, "")
            return cmpState
        }
        cmpUserConsentRepository.saveUserConsent(jsonData)
        cmpState = CmpState.consentSaved()
        cmpStatusRepository.saveState(
            cmpState
        )
        return cmpState
    }

    fun lastConsentUpdate() {
        cmpStatusRepository.getLastConsentGiven()
    }

    fun isOpen() {
        //cmpStatusRepository.isLayerOpen()
    }

    fun getState(): CmpState {
        val cmpState: CmpState? = cmpStatusRepository.getState()

        if(cmpState == null) {
            Napier.e("Error, no user consent given", tag = TAG)
            return CmpState.error(
                CmpLayer.LAYER_CLOSED,
                "No State found"
            )
        }
        return cmpState
    }

    fun getAddtlString(): String {
        return cmpUserConsentRepository.getUserConsent().addTlConsent
    }

    fun getUsPrivacyString(): String {
        return cmpUserConsentRepository.getUserConsent().usPrivacyString
    }

    fun getLastRequest(): String {
        return cmpStatusRepository.getLastConsentGiven().toString() ?: ""
    }
}