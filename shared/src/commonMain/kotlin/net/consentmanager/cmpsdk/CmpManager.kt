package net.consentmanager.cmpsdk

import io.github.aakira.napier.DebugAntilog
import io.github.aakira.napier.Napier
import net.consentmanager.cmpsdk.eventListener.CmpEventListener
import net.consentmanager.cmpsdk.service.CmpUserConsentService
import net.consentmanager.cmpsdk.utils.CmpUrlBuilder
import org.kodein.di.*

private const val TAG = "CMP"

class CmpManager private constructor(
    private val config: CmpConfig,
    private var cmpEventListener: CmpEventListener,
) : CmpManagerInterface {
    private val di =  LateInitDI()
    private val cmpUserConsentServiceProvider:
                () -> CmpUserConsentService by di.provider<CmpUserConsentService>()
    private var cmpUserConsentService: CmpUserConsentService

    init {
        this.di.baseDI = serviceDi
        this.cmpUserConsentService = cmpUserConsentServiceProvider.invoke()
        Napier.base(DebugAntilog())
    }

    fun setConsentEventListener(cmpLayerEventListener: CmpEventListener): CmpManager {
        this.cmpEventListener = cmpLayerEventListener
        return this
    }

    /**
     * Initialize manager and check current state. Will open Consent Layer if needed
     */
    fun initialize(): CmpManager {
        validateManagerOnInit()
        return this
    }
    /**
     * import a consent String.
     */
    fun importConsentString(consent: String): CmpManager {
        try {
            cmpUserConsentService.importConsentString(consent)
            cmpEventListener.userConsentImported(consent)
        } catch (e: IllegalStateException) {

        }
        return this
    }

    /**
     * export a consent String. Empty when no Consent saved
     */
    fun exportConsentString(): String {
        return cmpUserConsentService.getUserConsentString()
    }

    /**
     * reset manager state and delete all consent data
     */
    fun reset() {
        cmpUserConsentService.resetUserConsent()
    }

    /**
     * open Cmp Layer
     */
    override fun openCmpLayer() {
        cmpEventListener.consentLayerOpened()
        val consent : String = cmpUserConsentService.getUserConsentString()
        val url: String = CmpUrlBuilder().setConfig(config).setConsent(consent).setOpenLayer(true).build()
        // -> cmpUserConsentService should change state to open cmp
        // TODO
        // change state
        // open UI
    }

    /**
     * check if user gave consent for given vendor
     */
    override fun hasVendor(id: String): Boolean {
        return cmpUserConsentService.hasVendor(id)
    }

    /**
     * check if user gave consent for given purpose
     */
    override fun hasPurpose(id: String): Boolean {
        return cmpUserConsentService.hasPurpose(id)
    }

    /**
     * TODO check if needed
     */
    override fun hasPurposeForVendor(purposeId: String, vendorId: String): Boolean {
        return cmpUserConsentService.hasPurposeForVendor(purposeId, vendorId)
    }

    /**
     * returns the id list of given consent purpose
     */
    override fun getPurposes(): List<String> {
        return cmpUserConsentService.getPurposeList()
    }

    /**
     * returns the id list of given consent vendors
     */
    override fun getVendors(): List<String> {
        return cmpUserConsentService.getVendors()
    }

    override fun getGoogleAddtlConsent(): String {
        return cmpUserConsentService.getAddtlString()
    }

    override fun getUsPrivacyString(): String {
        return cmpUserConsentService.getUsPrivacyString()
    }

    override fun getLastRequest(): String {
        return cmpUserConsentService.getLastRequest()
    }

    /**
     * return information if the user need to give consent
     */
    override fun needUserConsent(): Boolean {
        return cmpUserConsentService.needUserConsent()
    }

    // Placeholder feature

    /**
     * creates Placeholder UI View
     */
    override fun createPlaceholderView() {
        TODO("Not yet implemented")
    }

    // TODO placeholder functions


    // private helper functions
    private fun validateManagerOnInit() {
        //TODO validation about the state of the manager when initialize
    }

    override fun toString(): String {
        return "CmpManager(config=$config, cmpLayerEventListener=$cmpEventListener)"
    }

    companion object {

        fun create(
            id: String,
            domain: String,
            appName: String,
            language: String,
        ): CmpManager {
            val config: CmpConfig = CmpConfig.create(id, domain, appName, language)!!
            return CmpManager(
                config,
                object : CmpEventListener {

                },
            )
        }

        fun create(config: CmpConfig): CmpManager {
            return CmpManager(
                config,
                object : CmpEventListener {

                },
            )
        }

        fun create(
            id: String,
            domain: String,
            appName: String,
            language: String,
            di: DI
        ): CmpManager {
            val config: CmpConfig = CmpConfig.create(id, domain, appName, language)!!
            return CmpManager(
                config,
                object : CmpEventListener {

                },
                di
            )
        }
    }

    constructor(config: CmpConfig, cmpEventListener: CmpEventListener, di: DI) : this(config,
        cmpEventListener)
}