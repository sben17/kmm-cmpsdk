package net.consentmanager.cmpsdk

interface CmpManagerInterface {
    fun hasVendor(id: String): Boolean
    fun hasPurpose(id: String): Boolean
    fun hasPurposeForVendor(purposeId: String, vendorId: String): Boolean
    fun getPurposes(): List<String>
    fun getVendors(): List<String>
    fun getGoogleAddtlConsent(): String
    fun getUsPrivacyString(): String
    fun getLastRequest(): String
    fun openCmpLayer()
    fun needUserConsent(): Boolean

    // Placeholder
    fun createPlaceholderView()
}