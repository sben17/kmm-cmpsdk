package net.consentmanager.cmpsdk.utils

class CmpPlaceholderParameter private constructor(private val vendorId: String) {
    private var text: String? = null
    private var headline: String? = null
    private var buttonText: String? = null
    private var checkboxText: String? = null
    private var imageUrl: String? = null

    companion object {
        fun ofVendor(id: String): CmpPlaceholderParameter? {
            return CmpPlaceholderParameter(id)
        }
    }

    fun setCustomPlaceholder(
        headline: String?,
        mainText: String?,
        checkboxText: String?,
        buttonText: String?
    ): CmpPlaceholderParameter {
        this.buttonText = buttonText
        this.headline = headline
        this.text = mainText
        this.checkboxText = checkboxText
        return this
    }

    fun addOptionalImageUrl(imageUrl: String?): CmpPlaceholderParameter? {
        this.imageUrl = imageUrl
        return this
    }

    val customPlaceholderToGetParam: String
        get() {
            val buttonTextParam = "&btn="
            val checkboxParam = "&check="
            val textParam = "&txt="
            val headlineParam = "&hl="
            val url = StringBuilder()
            if (buttonText != null) {
                url.append("$buttonTextParam$buttonText")
            }
            if (checkboxText != null) {
                url.append("$checkboxParam$checkboxText")
            }
            if (text != null) {
                url.append("$textParam$text")
            }
            if (headline != null) {
                url.append("$headlineParam$headline")
            }
            return url.toString()
        }

    val imageUrlToGetParam: String
        get() = if (imageUrl != null) {
            "&img=%$imageUrl"
        } else ""

    override fun toString(): String {
        return "CMPPlaceholderParams{" +
                "vendorId='" + vendorId + '\'' +
                ", text='" + text + '\'' +
                ", headline='" + headline + '\'' +
                ", buttonText='" + buttonText + '\'' +
                ", checkboxText='" + checkboxText + '\'' +
                ", imageUrl='" + imageUrl + '\'' +
                '}'
    }
}