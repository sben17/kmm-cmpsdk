package net.consentmanager.cmpsdk.utils

// category types applies to vendors and purposes
private const val CUSTOM_CATEGORY_PREFIX = "c"
private const val SYSTEM_CATEGORY_PREFIX = "s"
private const val IAB_CATEGORY_PATTERN = "^[1-9]+[0-9]*\$"

fun getVendorType(vendorId: String): VendorTypeEnum {
    val pattern: Regex = IAB_CATEGORY_PATTERN.toRegex()

    if (pattern.matches(vendorId)) {
        return VendorTypeEnum.IAB_VENDOR
    }
    if (vendorId.startsWith(CUSTOM_CATEGORY_PREFIX)) {
        return VendorTypeEnum.CUSTOM_VENDOR
    }
    if (vendorId.startsWith(SYSTEM_CATEGORY_PREFIX)) {
        return VendorTypeEnum.SYSTEM_VENDOR
    }
    throw IllegalArgumentException("Id $vendorId is not a valid vendor")
}

fun isValidVendor(vendorId: String): Boolean {
    val pattern: Regex = IAB_CATEGORY_PATTERN.toRegex()
    return pattern.matches(vendorId)
            || vendorId.startsWith(CUSTOM_CATEGORY_PREFIX)
            || vendorId.startsWith(SYSTEM_CATEGORY_PREFIX)
}

fun isValidPurpose(purposeId: String): Boolean {
    val pattern: Regex = IAB_CATEGORY_PATTERN.toRegex()
    return pattern.matches(purposeId)
            || purposeId.startsWith(CUSTOM_CATEGORY_PREFIX)
            || purposeId.startsWith(SYSTEM_CATEGORY_PREFIX)
}

fun getPurposeType(purposeId: String): PurposeTypeEnum {
    val pattern: Regex = IAB_CATEGORY_PATTERN.toRegex()

    if (pattern.matches(purposeId)) {
        return PurposeTypeEnum.IAB_PURPOSE
    }
    if (purposeId.startsWith(CUSTOM_CATEGORY_PREFIX)) {
        return PurposeTypeEnum.CUSTOM_PURPOSE
    }
    if (purposeId.startsWith(SYSTEM_CATEGORY_PREFIX)) {
        return PurposeTypeEnum.SYSTEM_PURPOSE
    }
    throw IllegalArgumentException("Id $purposeId is not a valid vendor")
}

enum class VendorTypeEnum {
    IAB_VENDOR,
    SYSTEM_VENDOR,
    CUSTOM_VENDOR;
}

enum class PurposeTypeEnum {
    IAB_PURPOSE,
    SYSTEM_PURPOSE,
    CUSTOM_PURPOSE;
}