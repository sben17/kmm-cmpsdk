package net.consentmanager.cmpsdk.utils

import android.annotation.SuppressLint
import net.consentmanager.cmpsdk.CmpConfig
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*

class CmpUrlBuilder {
    private var id: String? = null
    private var domain: String? = null
    private var idfa: String? = null
    private var language: String? = null
    private var appName: String? = null
    private var consent: String? = ""
    private var openView = true

    fun build(): String {
        require(!(id == null || domain == null)) { "You need to provide at least the app id and the specific consentmanager domain" }
        val outcome: StringBuilder = StringBuilder("https://$domain/delivery/appcmp2.php?id=$id")
        if (idfa != null) {
            outcome.append("&idfa=$idfa")
        }
        if (language != null) {
            outcome.append("&l=$language")
        }
        if (appName != null) {
            outcome.append("&appname=$appName")
        }
        if (consent != null && consent != "") {
            outcome.append(
                "&zt=$dateAndRandomNumberAsString#cmpimport=$consent",
            )
        }
        if (openView) {
            outcome.append("&cmpscreen")
        }
        return outcome.toString()
    }

    fun setConfig(config: CmpConfig): CmpUrlBuilder {
        language = config.getLanguage()
        appName = config.getAppName()
        id = config.getId()
        domain = config.getServerDomain()
        return this
    }

    fun setConsent(consent: String?): CmpUrlBuilder {
        if (consent == null) {
            return this
        }
        this.consent = consent
        return this
    }

    fun setIdfa(idfa: String?): CmpUrlBuilder {
        this.idfa = idfa
        return this
    }

    fun setOpenLayer(justOpenLayer: Boolean): CmpUrlBuilder {
        openView = justOpenLayer
        return this
    }

    private val dateAndRandomNumberAsString: String
        @SuppressLint("SimpleDateFormat")
        get() {
            val rand = Random()
            val range = 10000
            val dateFormat: DateFormat =
                SimpleDateFormat("ddMMyyyy")
            // TODO datetime KMM
            val strDate = ""
            //dateFormat.format(Date())
            return "$strDate${rand.nextInt(range)}"
        }

    companion object {
        private const val URL = "https://%s/delivery/appcmp2.php?id=%s"
    }
}
