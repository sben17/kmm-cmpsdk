package net.consentmanager.cmpsdk.infrastructure

import kotlinx.datetime.Instant
import net.consentmanager.cmpsdk.model.CmpState

interface CmpStatusRepositoryInterface {

    // writer
    fun saveLastConsentGiven()
    fun saveState(state: CmpState)

    // getter
    fun getState(): CmpState?
    fun getLastConsentGiven(): Instant?
}