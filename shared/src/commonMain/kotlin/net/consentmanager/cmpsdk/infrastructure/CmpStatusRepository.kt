package net.consentmanager.cmpsdk.infrastructure

import com.russhwolf.settings.Settings
import kotlinx.datetime.Clock
import kotlinx.datetime.Instant
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import net.consentmanager.cmpsdk.model.CmpState

private const val CMP_LAST_CONSENT: String = "CMP_LAST_CONSENT"
private const val CMP_STATE_ID: String = "CMP_STATE_ID"

class CmpStatusRepository(
    private val settings: Settings
) : CmpStatusRepositoryInterface {

    override fun saveLastConsentGiven() {
        settings.putString(CMP_LAST_CONSENT, Clock.System.now().toString())
    }

    override fun saveState(state: CmpState) {
        val encoded : String = Json.encodeToString(state)
        settings.putString(CMP_STATE_ID, encoded)
    }

    override fun getState(): CmpState? {
        val jsonConfig = Json { isLenient = true; ignoreUnknownKeys = true }
        val jsonData: String = settings.getString(CMP_STATE_ID)
        if (jsonData.isEmpty()) {
            return null
        }
        return Json.decodeFromString<CmpState>(jsonData)
    }

    override fun getLastConsentGiven(): Instant? {
        val lastConsent :String = settings.getString(CMP_LAST_CONSENT)
        if(lastConsent == "")
            return null
        return  Instant.parse(lastConsent)
    }

}