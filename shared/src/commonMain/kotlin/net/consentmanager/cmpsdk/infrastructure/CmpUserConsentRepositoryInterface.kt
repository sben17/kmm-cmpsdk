package net.consentmanager.cmpsdk.infrastructure

import net.consentmanager.cmpsdk.model.CmpUserConsent

interface CmpUserConsentRepositoryInterface {
    fun saveUserConsent(json: String)
    fun getUserConsent(): CmpUserConsent
    fun removeUserConsent()
}