package net.consentmanager.cmpsdk.infrastructure

import com.russhwolf.settings.Settings
import io.github.aakira.napier.Napier
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.json.Json
import net.consentmanager.cmpsdk.model.CmpUserConsent

private const val ID: String = "CMP_CONSENT_ID"

class CmpUserConsentRepository(private val settings: Settings) : CmpUserConsentRepositoryInterface{

        override fun saveUserConsent(json: String) {
            val jsonConfig = Json { isLenient = true; ignoreUnknownKeys = true }
            val jsonData: String = settings.getString(ID)
            if(jsonData.isEmpty()) {
                return
            }
            val cmp : CmpUserConsent =  jsonConfig.decodeFromString(settings.getString(ID))
            saveUserConsentMapper(cmp)
            settings.putString(ID, json)
        }

        override fun getUserConsent(): CmpUserConsent {
            val jsonConfig = Json { isLenient = true; ignoreUnknownKeys = true }
            val jsonData: String = settings.getString(ID)
            if(jsonData.isEmpty()) {
                Napier.e("User Consent Data is empty.")
                return CmpUserConsent.createEmpty()
            }
            return jsonConfig.decodeFromString(settings.getString(ID))
        }

        override fun removeUserConsent() {
            settings.remove(ID)
        }

        private fun saveUserConsentMapper(cmpUserConsent: CmpUserConsent) {
            settings.putString(CONSENT_STRING, cmpUserConsent.consentString)
            settings.putString(REGULATION_STATUS, cmpUserConsent.regulationKey)
            settings.putInt(GDPR_APPLIES, cmpUserConsent.regulation)
            settings.putString(PUBLISHER_CC, cmpUserConsent.publisherCC)
            settings.putInt(POLICY_VERSION, 2)
            settings.putString(ADDTL_CONSENT, cmpUserConsent.addTlConsent)
            settings.putString(US_PRIVACY_STRING, cmpUserConsent.usPrivacyString)
        }
}
// implemented
private const val POLICY_VERSION = "IABTCF_PolicyVersion"
private const val GDPR_APPLIES = "IABTCF_gdprApplies"
private const val PUBLISHER_CC = "IABTCF_PublisherCC"
private const val REGULATION_STATUS = "CMP_REGULATION_STATUS"
// new ones
private const val ADDTL_CONSENT = "IABTCF_AddtlConsent"
private const val US_PRIVACY_STRING = "IABUSPrivacy_String"
private const val CONSENT_STRING = "IABConsent_ConsentString"

// TODO Missing
private const val TC_STRING = "IABTCF_TCString"
private const val VENDOR_CONSENTS = "IABTCF_VendorConsents"
private const val VENDOR_LEGITIMATE_INTERESTS = "IABTCF_VendorLegitimateInterests"
private const val PURPOSE_CONSENTS = "IABTCF_PurposeConsents"
private const val PURPOSE_LEGITIMATE_INTERESTS = "IABTCF_PurposeLegitimateInterests"
private const val SPECIAL_FEATURES_OPT_INS = "IABTCF_SpecialFeaturesOptIns"
private const val PUBLISHER_RESTRICTIONS = "IABTCF_PublisherRestrictions%d" //%d = Purpose ID

private const val PUBLISHER_CONSENT = "IABTCF_PublisherConsent"
private const val PUBLISHER_LEGITIMATE_INTERESTS = "IABTCF_PublisherLegitimateInterests"
private const val PUBLISHER_CUSTOM_PURPOSES_CONSENTS = "IABTCF_PublisherCustomPurposesConsents"
private const val PUBLISHER_CUSTOM_PURPOSES_LEGITIMATE_INTERESTS =
    "IABTCF_PublisherCustomPurposesLegitimateInterests"
private const val PURPOSE_ONE_TREATMENT = "IABTCF_PurposeOneTreatment"
private const val USE_NONE_STANDARD_STACKS = "IABTCF_UseNonStandardStacks"


// TODO state service
private const val CMP_SDK_ID = "IABTCF_CmpSdkID"
private const val CMP_SDK_VERSION = "IABTCF_CmpSdkVersion"

