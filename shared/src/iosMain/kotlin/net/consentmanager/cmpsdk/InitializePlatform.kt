package net.consentmanager.cmpsdk

import io.github.aakira.napier.DebugAntilog
import io.github.aakira.napier.Napier

actual class InitializePlatform {
    actual fun init() {
        Napier.base(DebugAntilog())
    }
}