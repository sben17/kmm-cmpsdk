package net.consentmanager.cmpsdk

import com.russhwolf.settings.MockSettings
import com.russhwolf.settings.Settings
import net.consentmanager.cmpsdk.eventListener.CmpEventListener
import net.consentmanager.cmpsdk.infrastructure.CmpStatusRepository
import net.consentmanager.cmpsdk.infrastructure.CmpStatusRepositoryInterface
import net.consentmanager.cmpsdk.infrastructure.CmpUserConsentRepository
import net.consentmanager.cmpsdk.infrastructure.CmpUserConsentRepositoryInterface
import net.consentmanager.cmpsdk.service.CmpUserConsentService
import org.kodein.di.*
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertNotEquals

class CmpManagerTests: DIAware {

    @Test
    fun testConstructWithConfig() {
        val config : CmpConfig = CmpConfig.create("12", "consentmanager.net", "testApp", "DE")!!
        val cmpManager : CmpManager = CmpManager.create(config)
        assertNotEquals("", cmpManager.toString())
    }

    @Test
    fun testConstructorSimple() {
        val cmpManager : CmpManager = CmpManager.create("12", "consentmanager.net", "testApp", "DE")
        assertNotEquals("", cmpManager.toString())
    }

    @Test
    fun testAddEventListener() {
        val config : CmpConfig = CmpConfig.create("12", "consentmanager.net", "testApp", "DE")!!
        var check = 0
        // test if custom evenListener is called and changes check values on openCmpLayer()
        val cmpManager : CmpManager = CmpManager.create(config).setConsentEventListener(
            object : CmpEventListener {
                override fun consentLayerOpened() {
                    check = 2
                }
            }
        )
        cmpManager.openCmpLayer()
        assertEquals(2, check)
    }

    @Test
    fun testImportConsentString() {
        val cmpManager : CmpManager = CmpManager.create("12", "consentmanager.net", "testApp", "DE", di)
        cmpManager.importConsentString("test")
        // cmpManager should import String _ opens ui element with consent and saves consent data locally
        assertEquals("test", cmpManager.exportConsentString())
    }

    override val di: DI
        get() =  DI {
            bind<Settings> { singleton { MockSettings() }}
            bind<CmpStatusRepositoryInterface> { singleton { CmpStatusRepository(instance()) }}
            bind<CmpUserConsentRepositoryInterface> { singleton { CmpUserConsentRepository(instance()) }}
            bind<CmpUserConsentService> { singleton { CmpUserConsentService(instance(), instance()) }}
        }

}