package net.consentmanager.cmpsdk.infrastructure

import com.russhwolf.settings.MockSettings
import net.consentmanager.cmpsdk.model.CmpUserConsent
import kotlin.test.*

class CmpUserConsentRepositoryTests {

    @Test
    fun testSaveAndGetUserConsent() {
        val cmpUserConsentRepository : CmpUserConsentRepository = CmpUserConsentRepository(MockSettings())
        cmpUserConsentRepository.saveUserConsent(getExampleJson())
        val userConsent : CmpUserConsent? = cmpUserConsentRepository.getUserConsent()
        assertNotNull(userConsent)
        assertEquals<Any>(expected = 1, actual = userConsent.regulation)
        assertEquals("1YNN", userConsent.usPrivacyString)
        assertTrue(userConsent.gdprApplies)
        assertTrue(userConsent.tcfCompliant)
        assertFalse(userConsent.hasGlobalScope)
    }

    @Test
    fun testRemoveUserConsent() {
        val cmpUserConsentRepository : CmpUserConsentRepository = CmpUserConsentRepository(MockSettings())
        cmpUserConsentRepository.saveUserConsent(getExampleJson())
        cmpUserConsentRepository.removeUserConsent()
        val userConsent : CmpUserConsent? = cmpUserConsentRepository.getUserConsent()
        assertNull(userConsent)
    }
}

private fun getExampleJson(): String {
    return """
            {
              "cmpId": "",
              "consentstring": "CPVJUdgPVJUdgAfZXDDECECsAP_AAH_AAAigIXBV5D7NDWFFUXx7SttkWYQX1sQVI2QCChCAA6AFADGA8KQC00ASsAyABCACIQgAoRYBAAAEHAFEAECQQIBEARHkAwQEgAAIICAEABEQQkIQAAoKAAAAAAAIAAARKwCAkEDQA8bmRHAA1IAwQAgAgIABAIADAhMAWEAYAAACAAIAAABAAgIAEELgq8h9mhrCiqL49pW2yLMIL62IKkbIBBQhAAdACgBjAeFIBaaAJWAZAAhABEIQAUIsAgAACDgCiACBIIEAiAIjyAYICQAAEEBACAAiIISEIAAUFAAAAAAAEAAAIlYBASCBoAeNzIjgAakAYIAQAQEAAgEABgQmALCAMAAABAAEAAAAgAQEACAoCgAgEYBgAgAVAFiCQAQCMBQAQAKgCxBoAIBGA4AIAFQBYhEAEAjAkAEACoAsQqACARgWACABUAWIZABAIwNABAAqALEOgAgEYHgAgAVAFiIQAQCMEQAQAKgCxEoAIBGCYAIAFQBYikAEAjBUAEACoAsQ",
              "uspstring": "1YNN",
              "gdprApplies": true,
              "hasGlobalScope": false,
              "tcfversion": 2,
              "tcfcompliant": true,
              "regulation": 1,
              "regulationKey": "GDPR",
              "purposeConsents": {
                "1": true,
                "2": true,
                "3": true,
                "4": true,
                "5": true,
                "6": true,
                "7": true,
                "8": true,
                "9": true,
                "10": true,
                "c1": true,
                "s1": true,
                "s2": true,
                "r1": true,
                "r2": true,
                "c19": true
              },
              "vendorConsents": {
                "4": true,
                "6": true,
                "8": true,
                "10": true,
                "11": true,
                "12": true,
                "13": true,
                "16": true,
                "21": true,
                "22": true,
                "23": true,
                "24": true,
                "25": true,
                "27": true,
                "28": true,
                "31": true,
                "32": true,
                "34": true,
                "39": true,
                "40": true,
                "42": true,
                "44": true,
                "45": true,
                "50": true,
                "52": true,
                "56": true,
                "58": true,
                "60": true,
                "62": true,
                "66": true,
                "68": true,
                "69": true,
                "70": true,
                "71": true,
                "72": true,
                "76": true,
                "77": true,
                "78": true,
                "79": true,
                "81": true,
                "82": true,
                "84": true,
                "87": true,
                "89": true,
                "91": true,
                "92": true,
                "94": true,
                "95": true,
                "97": true,
                "98": true,
                "100": true,
                "101": true,
                "104": true,
                "108": true,
                "110": true,
                "111": true,
                "114": true,
                "115": true,
                "120": true,
                "126": true,
                "128": true,
                "129": true,
                "130": true,
                "131": true,
                "132": true,
                "134": true,
                "136": true,
                "137": true,
                "139": true,
                "140": true,
                "144": true,
                "150": true,
                "152": true,
                "154": true,
                "157": true,
                "161": true,
                "162": true,
                "164": true,
                "165": true,
                "168": true,
                "177": true,
                "183": true,
                "185": true,
                "190": true,
                "195": true,
                "209": true,
                "210": true,
                "211": true,
                "213": true,
                "224": true,
                "226": true,
                "237": true,
                "238": true,
                "242": true,
                "243": true,
                "251": true,
                "252": true,
                "253": true,
                "254": true,
                "259": true,
                "261": true,
                "264": true,
                "273": true,
                "275": true,
                "276": true,
                "278": true,
                "281": true,
                "282": true,
                "284": true,
                "294": true,
                "297": true,
                "299": true,
                "301": true,
                "302": true,
                "311": true,
                "312": true,
                "315": true,
                "328": true,
                "333": true,
                "345": true,
                "349": true,
                "354": true,
                "359": true,
                "371": true,
                "373": true,
                "378": true,
                "382": true,
                "384": true,
                "385": true,
                "394": true,
                "416": true,
                "422": true,
                "423": true,
                "424": true,
                "434": true,
                "436": true,
                "440": true,
                "452": true,
                "459": true,
                "462": true,
                "468": true,
                "475": true,
                "484": true,
                "488": true,
                "498": true,
                "502": true,
                "506": true,
                "507": true,
                "508": true,
                "509": true,
                "512": true,
                "521": true,
                "522": true,
                "528": true,
                "536": true,
                "539": true,
                "559": true,
                "565": true,
                "573": true,
                "584": true,
                "598": true,
                "602": true,
                "606": true,
                "612": true,
                "617": true,
                "620": true,
                "625": true,
                "630": true,
                "647": true,
                "649": true,
                "655": true,
                "657": true,
                "703": true,
                "726": true,
                "730": true,
                "733": true,
                "735": true,
                "737": true,
                "738": true,
                "747": true,
                "755": true,
                "758": true,
                "764": true,
                "771": true,
                "772": true,
                "774": true,
                "785": true,
                "786": true,
                "787": true,
                "788": true,
                "792": true,
                "793": true,
                "795": true,
                "796": true,
                "797": true,
                "800": true,
                "801": true,
                "804": true,
                "808": true,
                "812": true,
                "813": true,
                "814": true,
                "827": true,
                "828": true,
                "830": true,
                "832": true,
                "835": true,
                "845": true,
                "846": true,
                "852": true,
                "863": true,
                "875": true,
                "883": true,
                "898": true,
                "907": true,
                "921": true,
                "922": true,
                "929": true,
                "934": true,
                "937": true,
                "938": true,
                "948": true,
                "950": true,
                "951": true,
                "956": true,
                "966": true,
                "967": true,
                "993": true,
                "1009": true,
                "1036": true,
                "1049": true,
                "1057": true,
                "1070": true,
                "s1227": true,
                "s218": true,
                "c5975": true,
                "s23": true,
                "c5147": true,
                "s7": true,
                "c11367": true,
                "c5163": true,
                "s26": true,
                "s1104": true,
                "s14": true,
                "c12756": true,
                "c4499": true,
                "c5136": true,
                "c6925": true,
                "c7758": true,
                "c12084": true,
                "c5334": true,
                "c5158": true,
                "c5135": true,
                "s34": true,
                "s30": true
              },
              "purposeLI": {
                "1": true,
                "2": true,
                "3": true,
                "4": true,
                "5": true,
                "6": true,
                "7": true,
                "8": true,
                "9": true,
                "10": true,
                "c1": true,
                "s1": true,
                "s2": true,
                "r1": true,
                "r2": true,
                "c19": true
              },
              "vendorLI": {
                "4": true,
                "6": true,
                "8": true,
                "10": true,
                "11": true,
                "12": true,
                "13": true,
                "16": true,
                "21": true,
                "22": true,
                "23": true,
                "24": true,
                "25": true,
                "27": true,
                "28": true,
                "31": true,
                "32": true,
                "34": true,
                "39": true,
                "40": true,
                "42": true,
                "44": true,
                "45": true,
                "50": true,
                "52": true,
                "56": true,
                "58": true,
                "60": true,
                "62": true,
                "66": true,
                "68": true,
                "69": true,
                "70": true,
                "71": true,
                "72": true,
                "76": true,
                "77": true,
                "78": true,
                "79": true,
                "81": true,
                "82": true,
                "84": true,
                "87": true,
                "89": true,
                "91": true,
                "92": true,
                "94": true,
                "95": true,
                "97": true,
                "98": true,
                "100": true,
                "101": true,
                "104": true,
                "108": true,
                "110": true,
                "111": true,
                "114": true,
                "115": true,
                "120": true,
                "126": true,
                "128": true,
                "129": true,
                "130": true,
                "131": true,
                "132": true,
                "134": true,
                "136": true,
                "137": true,
                "139": true,
                "140": true,
                "144": true,
                "150": true,
                "152": true,
                "154": true,
                "157": true,
                "161": true,
                "162": true,
                "164": true,
                "165": true,
                "168": true,
                "177": true,
                "183": true,
                "185": true,
                "190": true,
                "195": true,
                "209": true,
                "210": true,
                "211": true,
                "213": true,
                "224": true,
                "226": true,
                "237": true,
                "238": true,
                "242": true,
                "243": true,
                "251": true,
                "252": true,
                "253": true,
                "254": true,
                "259": true,
                "261": true,
                "264": true,
                "273": true,
                "275": true,
                "276": true,
                "278": true,
                "281": true,
                "282": true,
                "284": true,
                "294": true,
                "297": true,
                "299": true,
                "301": true,
                "302": true,
                "311": true,
                "312": true,
                "315": true,
                "328": true,
                "333": true,
                "345": true,
                "349": true,
                "354": true,
                "359": true,
                "371": true,
                "373": true,
                "378": true,
                "382": true,
                "384": true,
                "385": true,
                "394": true,
                "416": true,
                "422": true,
                "423": true,
                "424": true,
                "434": true,
                "436": true,
                "440": true,
                "452": true,
                "459": true,
                "462": true,
                "468": true,
                "475": true,
                "484": true,
                "488": true,
                "498": true,
                "502": true,
                "506": true,
                "507": true,
                "508": true,
                "509": true,
                "512": true,
                "521": true,
                "522": true,
                "528": true,
                "536": true,
                "539": true,
                "559": true,
                "565": true,
                "573": true,
                "584": true,
                "598": true,
                "602": true,
                "606": true,
                "612": true,
                "617": true,
                "620": true,
                "625": true,
                "630": true,
                "647": true,
                "649": true,
                "655": true,
                "657": true,
                "703": true,
                "726": true,
                "730": true,
                "733": true,
                "735": true,
                "737": true,
                "738": true,
                "747": true,
                "755": true,
                "758": true,
                "764": true,
                "771": true,
                "772": true,
                "774": true,
                "785": true,
                "786": true,
                "787": true,
                "788": true,
                "792": true,
                "793": true,
                "795": true,
                "796": true,
                "797": true,
                "800": true,
                "801": true,
                "804": true,
                "808": true,
                "812": true,
                "813": true,
                "814": true,
                "827": true,
                "828": true,
                "830": true,
                "832": true,
                "835": true,
                "845": true,
                "846": true,
                "852": true,
                "863": true,
                "875": true,
                "883": true,
                "898": true,
                "907": true,
                "921": true,
                "922": true,
                "929": true,
                "934": true,
                "937": true,
                "938": true,
                "948": true,
                "950": true,
                "951": true,
                "956": true,
                "966": true,
                "967": true,
                "993": true,
                "1009": true,
                "1036": true,
                "1049": true,
                "1057": true,
                "1070": true,
                "s1227": true,
                "s218": true,
                "c5975": true,
                "s23": true,
                "c5147": true,
                "s7": true,
                "c11367": true,
                "c5163": true,
                "s26": true,
                "s1104": true,
                "s14": true,
                "c12756": true,
                "c4499": true,
                "c5136": true,
                "c6925": true,
                "c7758": true,
                "c12084": true,
                "c5334": true,
                "c5158": true,
                "c5135": true,
                "s34": true,
                "s30": true
              },
              "googleVendorConsents": {
                "4": true,
                "12": true,
                "22": true,
                "48": true,
                "62": true,
                "76": true,
                "80": true,
                "85": true,
                "86": true,
                "89": true,
                "126": true,
                "134": true,
                "154": true,
                "161": true,
                "162": true,
                "165": true,
                "178": true,
                "209": true,
                "221": true,
                "271": true,
                "274": true,
                "294": true,
                "303": true,
                "310": true,
                "313": true,
                "314": true,
                "316": true,
                "317": true,
                "348": true,
                "359": true,
                "363": true,
                "398": true,
                "432": true,
                "443": true,
                "459": true,
                "469": true,
                "479": true,
                "510": true,
                "528": true,
                "537": true,
                "571": true,
                "575": true,
                "587": true,
                "588": true,
                "590": true,
                "595": true,
                "624": true,
                "776": true,
                "782": true,
                "797": true,
                "814": true,
                "932": true,
                "933": true,
                "976": true,
                "986": true,
                "987": true,
                "991": true,
                "1028": true,
                "1029": true,
                "1063": true,
                "1072": true,
                "1126": true,
                "1171": true,
                "1172": true,
                "1178": true,
                "1225": true,
                "1232": true,
                "1236": true,
                "1241": true,
                "1248": true,
                "1313": true,
                "1329": true,
                "1344": true,
                "1419": true,
                "1428": true,
                "1514": true,
                "1517": true,
                "1520": true,
                "1542": true,
                "1547": true,
                "1652": true,
                "1674": true,
                "1684": true,
                "1705": true,
                "1716": true,
                "1735": true,
                "1741": true,
                "1776": true,
                "1801": true,
                "1858": true,
                "1872": true,
                "1875": true,
                "1892": true,
                "1905": true,
                "1911": true,
                "1945": true,
                "2079": true,
                "2170": true,
                "2247": true,
                "2271": true,
                "2299": true,
                "2346": true,
                "2411": true,
                "2422": true,
                "2464": true,
                "2586": true,
                "2632": true,
                "2699": true,
                "2702": true,
                "2704": true,
                "2705": true,
                "2706": true,
                "2709": true,
                "2777": true,
                "2778": true,
                "2779": true,
                "2780": true,
                "2802": true,
                "2808": true,
                "2810": true,
                "2822": true,
                "2829": true,
                "2847": true,
                "2899": true,
                "2910": true,
                "2928": true,
                "2971": true,
                "2977": true,
                "3001": true,
                "3015": true,
                "3020": true,
                "3031": true,
                "3107": true,
                "3166": true,
                "3181": true,
                "3199": true,
                "3206": true
              },
              "vendorsList": [
                {
                  "id": 948,
                  "iabid": 948,
                  "systemid": "s2408",
                  "googleid": 0,
                  "name": "1Agency"
                },
                {
                  "id": 92,
                  "iabid": 92,
                  "systemid": "s1185",
                  "googleid": 3031,
                  "name": "1plusX AG"
                },
                {
                  "id": 58,
                  "iabid": 58,
                  "systemid": "s1143",
                  "googleid": 4,
                  "name": "33Across"
                },
                {
                  "id": 40,
                  "iabid": 40,
                  "systemid": "s1222",
                  "googleid": 0,
                  "name": "Active Agent (ADITION technologies GmbH)"
                },
                {
                  "id": 830,
                  "iabid": 830,
                  "systemid": "s1574",
                  "googleid": 0,
                  "name": "Acxiom"
                },
                {
                  "id": 788,
                  "iabid": 788,
                  "systemid": "s1495",
                  "googleid": 0,
                  "name": "Ad Alliance GmbH"
                },
                {
                  "id": 649,
                  "iabid": 649,
                  "systemid": "s1224",
                  "googleid": 0,
                  "name": "adality GmbH"
                },
                {
                  "id": 738,
                  "iabid": 738,
                  "systemid": "s1405",
                  "googleid": 0,
                  "name": "adbility media GmbH"
                },
                {
                  "id": 299,
                  "iabid": 299,
                  "systemid": "s931",
                  "googleid": 1232,
                  "name": "AdClear GmbH"
                },
                {
                  "id": 539,
                  "iabid": 539,
                  "systemid": "s1226",
                  "googleid": 0,
                  "name": "AdDefend GmbH"
                },
                {
                  "id": "s1227",
                  "iabid": 0,
                  "systemid": "s1227",
                  "googleid": 0,
                  "name": "Adelphic LLC"
                },
                {
                  "id": 50,
                  "iabid": 50,
                  "systemid": "s911",
                  "googleid": 22,
                  "name": "Adform"
                },
                {
                  "id": 39,
                  "iabid": 39,
                  "systemid": "s1230",
                  "googleid": 0,
                  "name": "ADITION technologies GmbH"
                },
                {
                  "id": 813,
                  "iabid": 813,
                  "systemid": "s1522",
                  "googleid": 2822,
                  "name": "Adjust GmbH"
                },
                {
                  "id": 22,
                  "iabid": 22,
                  "systemid": "s936",
                  "googleid": 1514,
                  "name": "admetrics GmbH"
                },
                {
                  "id": 612,
                  "iabid": 612,
                  "systemid": "s959",
                  "googleid": 2702,
                  "name": "Adnami Aps"
                },
                {
                  "id": 264,
                  "iabid": 264,
                  "systemid": "s919",
                  "googleid": 359,
                  "name": "Adobe Advertising Cloud"
                },
                {
                  "id": 565,
                  "iabid": 565,
                  "systemid": "s1233",
                  "googleid": 0,
                  "name": "Adobe Audience Manager, Adobe Experience Platform"
                },
                {
                  "id": 224,
                  "iabid": 224,
                  "systemid": "s955",
                  "googleid": 2632,
                  "name": "adrule mobile GmbH"
                },
                {
                  "id": 6,
                  "iabid": 6,
                  "systemid": "s916",
                  "googleid": 85,
                  "name": "AdSpirit GmbH"
                },
                {
                  "id": 66,
                  "iabid": 66,
                  "systemid": "s996",
                  "googleid": 0,
                  "name": "adsquare GmbH"
                },
                {
                  "id": 507,
                  "iabid": 507,
                  "systemid": "s1132",
                  "googleid": 3199,
                  "name": "AdsWizz Inc."
                },
                {
                  "id": 211,
                  "iabid": 211,
                  "systemid": "s1237",
                  "googleid": 0,
                  "name": "AdTheorent, Inc"
                },
                {
                  "id": 827,
                  "iabid": 827,
                  "systemid": "s1560",
                  "googleid": 0,
                  "name": "Adtriba GmbH"
                },
                {
                  "id": 195,
                  "iabid": 195,
                  "systemid": "s926",
                  "googleid": 932,
                  "name": "advanced store GmbH"
                },
                {
                  "id": 27,
                  "iabid": 27,
                  "systemid": "s932",
                  "googleid": 1248,
                  "name": "ADventori SAS"
                },
                {
                  "id": 259,
                  "iabid": 259,
                  "systemid": "s1239",
                  "googleid": 0,
                  "name": "ADYOULIKE SA"
                },
                {
                  "id": 359,
                  "iabid": 359,
                  "systemid": "s554",
                  "googleid": 2271,
                  "name": "AerServ LLC"
                },
                {
                  "id": 785,
                  "iabid": 785,
                  "systemid": "s1491",
                  "googleid": 0,
                  "name": "agof studies"
                },
                {
                  "id": 793,
                  "iabid": 793,
                  "systemid": "s1053",
                  "googleid": 0,
                  "name": "Amazon Advertising"
                },
                {
                  "id": 922,
                  "iabid": 922,
                  "systemid": "s1638",
                  "googleid": 76,
                  "name": "Amnet GmbH"
                },
                {
                  "id": 23,
                  "iabid": 23,
                  "systemid": "s937",
                  "googleid": 1517,
                  "name": "Amobee Inc."
                },
                {
                  "id": 733,
                  "iabid": 733,
                  "systemid": "s1096",
                  "googleid": 0,
                  "name": "Anzu Virtual Reality LTD"
                },
                {
                  "id": 354,
                  "iabid": 354,
                  "systemid": "s1244",
                  "googleid": 0,
                  "name": "Apester Ltd"
                },
                {
                  "id": 797,
                  "iabid": 797,
                  "systemid": "s1506",
                  "googleid": 0,
                  "name": "Artefact Deutschland GmbH"
                },
                {
                  "id": "s218",
                  "iabid": 0,
                  "systemid": "s218",
                  "googleid": 587,
                  "name": "AT Internet"
                },
                {
                  "id": 394,
                  "iabid": 394,
                  "systemid": "s928",
                  "googleid": 991,
                  "name": "AudienceProject Aps"
                },
                {
                  "id": 598,
                  "iabid": 598,
                  "systemid": "s1251",
                  "googleid": 0,
                  "name": "audio content &amp; control GmbH"
                },
                {
                  "id": 956,
                  "iabid": 956,
                  "systemid": "s2416",
                  "googleid": 0,
                  "name": "Avantis Video Ltd"
                },
                {
                  "id": 907,
                  "iabid": 907,
                  "systemid": "s1625",
                  "googleid": 0,
                  "name": "AWIN AG"
                },
                {
                  "id": 647,
                  "iabid": 647,
                  "systemid": "s1256",
                  "googleid": 0,
                  "name": "Axel Springer Teaser Ad GmbH"
                },
                {
                  "id": 771,
                  "iabid": 771,
                  "systemid": "s1466",
                  "googleid": 3166,
                  "name": "bam! interactive marketing GmbH "
                },
                {
                  "id": 273,
                  "iabid": 273,
                  "systemid": "s938",
                  "googleid": 1547,
                  "name": "Bannerflow AB"
                },
                {
                  "id": 801,
                  "iabid": 801,
                  "systemid": "s1511",
                  "googleid": 0,
                  "name": "Bannernow, Inc."
                },
                {
                  "id": 12,
                  "iabid": 12,
                  "systemid": "s944",
                  "googleid": 1858,
                  "name": "BeeswaxIO Corporation"
                },
                {
                  "id": 87,
                  "iabid": 87,
                  "systemid": "s213",
                  "googleid": 571,
                  "name": "Betgenius Ltd"
                },
                {
                  "id": 462,
                  "iabid": 462,
                  "systemid": "s1266",
                  "googleid": 0,
                  "name": "Bidstack Limited"
                },
                {
                  "id": 128,
                  "iabid": 128,
                  "systemid": "s1004",
                  "googleid": 0,
                  "name": "BIDSWITCH GmbH"
                },
                {
                  "id": 185,
                  "iabid": 185,
                  "systemid": "s941",
                  "googleid": 1741,
                  "name": "Bidtellect, Inc"
                },
                {
                  "id": 625,
                  "iabid": 625,
                  "systemid": "s942",
                  "googleid": 1776,
                  "name": "BILENDI SA"
                },
                {
                  "id": 94,
                  "iabid": 94,
                  "systemid": "s929",
                  "googleid": 1063,
                  "name": "Blis Media Limited"
                },
                {
                  "id": 620,
                  "iabid": 620,
                  "systemid": "s17",
                  "googleid": 2699,
                  "name": "Blue"
                },
                {
                  "id": 422,
                  "iabid": 422,
                  "systemid": "s969",
                  "googleid": 2971,
                  "name": "Brand Metrics Sweden AB"
                },
                {
                  "id": 934,
                  "iabid": 934,
                  "systemid": "s1651",
                  "googleid": 0,
                  "name": "Brid Video DOO"
                },
                {
                  "id": 792,
                  "iabid": 792,
                  "systemid": "s1502",
                  "googleid": 0,
                  "name": "BritePool Inc"
                },
                {
                  "id": "c5975",
                  "iabid": 0,
                  "systemid": "c5975",
                  "googleid": 0,
                  "name": "Bugsnag"
                },
                {
                  "id": 315,
                  "iabid": 315,
                  "systemid": "s927",
                  "googleid": 987,
                  "name": "Celtra, Inc."
                },
                {
                  "id": 243,
                  "iabid": 243,
                  "systemid": "s1020",
                  "googleid": 0,
                  "name": "Cloud Technologies S.A."
                },
                {
                  "id": 416,
                  "iabid": 416,
                  "systemid": "s222",
                  "googleid": 595,
                  "name": "Commanders Act"
                },
                {
                  "id": 378,
                  "iabid": 378,
                  "systemid": "s827",
                  "googleid": 2910,
                  "name": "communicationAds GmbH &amp; Co. KG"
                },
                {
                  "id": 77,
                  "iabid": 77,
                  "systemid": "s92",
                  "googleid": 62,
                  "name": "Comscore B.V."
                },
                {
                  "id": 56,
                  "iabid": 56,
                  "systemid": "s1277",
                  "googleid": 0,
                  "name": "Confiant Inc."
                },
                {
                  "id": "s23",
                  "iabid": 0,
                  "systemid": "s23",
                  "googleid": 0,
                  "name": "consentmanager"
                },
                {
                  "id": 630,
                  "iabid": 630,
                  "systemid": "s1280",
                  "googleid": 0,
                  "name": "Contact Impact GmbH"
                },
                {
                  "id": 91,
                  "iabid": 91,
                  "systemid": "s115",
                  "googleid": 154,
                  "name": "Criteo SA"
                },
                {
                  "id": 875,
                  "iabid": 875,
                  "systemid": "s1581",
                  "googleid": 0,
                  "name": "cynapsis interactive GmbH"
                },
                {
                  "id": 573,
                  "iabid": 573,
                  "systemid": "s1286",
                  "googleid": 0,
                  "name": "Dailymotion SA"
                },
                {
                  "id": 938,
                  "iabid": 938,
                  "systemid": "s1666",
                  "googleid": 0,
                  "name": "dataXtrade GmbH"
                },
                {
                  "id": 440,
                  "iabid": 440,
                  "systemid": "s1022",
                  "googleid": 0,
                  "name": "DEFINE MEDIA GMBH"
                },
                {
                  "id": 209,
                  "iabid": 209,
                  "systemid": "s119",
                  "googleid": 165,
                  "name": "Delta Projects AB"
                },
                {
                  "id": 735,
                  "iabid": 735,
                  "systemid": "s1155",
                  "googleid": 0,
                  "name": "Deutsche Post AG"
                },
                {
                  "id": 144,
                  "iabid": 144,
                  "systemid": "s1142",
                  "googleid": 0,
                  "name": "district m inc."
                },
                {
                  "id": 852,
                  "iabid": 852,
                  "systemid": "s1558",
                  "googleid": 0,
                  "name": "diva-e products "
                },
                {
                  "id": 126,
                  "iabid": 126,
                  "systemid": "s122",
                  "googleid": 178,
                  "name": "DoubleVerify Inc.​"
                },
                {
                  "id": 434,
                  "iabid": 434,
                  "systemid": "s314",
                  "googleid": 1225,
                  "name": "DynAdmic"
                },
                {
                  "id": 584,
                  "iabid": 584,
                  "systemid": "s783",
                  "googleid": 2847,
                  "name": "Dynamic 1001 GmbH"
                },
                {
                  "id": 110,
                  "iabid": 110,
                  "systemid": "s156",
                  "googleid": 317,
                  "name": "Dynata LLC"
                },
                {
                  "id": 796,
                  "iabid": 796,
                  "systemid": "s1505",
                  "googleid": 0,
                  "name": "EASY Marketing GmbH"
                },
                {
                  "id": 168,
                  "iabid": 168,
                  "systemid": "s230",
                  "googleid": 776,
                  "name": "EASYmedia GmbH"
                },
                {
                  "id": 929,
                  "iabid": 929,
                  "systemid": "s1646",
                  "googleid": 0,
                  "name": "eBay Inc"
                },
                {
                  "id": 8,
                  "iabid": 8,
                  "systemid": "s234",
                  "googleid": 797,
                  "name": "Emerse Sverige AB"
                },
                {
                  "id": 213,
                  "iabid": 213,
                  "systemid": "s207",
                  "googleid": 537,
                  "name": "emetriq GmbH"
                },
                {
                  "id": 183,
                  "iabid": 183,
                  "systemid": "s336",
                  "googleid": 1329,
                  "name": "EMX Digital LLC"
                },
                {
                  "id": 24,
                  "iabid": 24,
                  "systemid": "s155",
                  "googleid": 316,
                  "name": "Epsilon"
                },
                {
                  "id": 814,
                  "iabid": 814,
                  "systemid": "s1523",
                  "googleid": 0,
                  "name": "Eskimi"
                },
                {
                  "id": "c5147",
                  "iabid": 0,
                  "systemid": "c5147",
                  "googleid": 0,
                  "name": "Eurosport "
                },
                {
                  "id": 312,
                  "iabid": 312,
                  "systemid": "s1292",
                  "googleid": 575,
                  "name": "Exactag GmbH"
                },
                {
                  "id": 120,
                  "iabid": 120,
                  "systemid": "s1293",
                  "googleid": 1428,
                  "name": "Eyeota Pte Ltd"
                },
                {
                  "id": "s7",
                  "iabid": 0,
                  "systemid": "s7",
                  "googleid": 89,
                  "name": "Facebook (Meta)"
                },
                {
                  "id": "c11367",
                  "iabid": 0,
                  "systemid": "c11367",
                  "googleid": 0,
                  "name": "Facebook Pixel"
                },
                {
                  "id": 795,
                  "iabid": 795,
                  "systemid": "s1504",
                  "googleid": 0,
                  "name": "Factor Eleven GmbH"
                },
                {
                  "id": 100,
                  "iabid": 100,
                  "systemid": "s1064",
                  "googleid": 0,
                  "name": "Fifty Technology Limited"
                },
                {
                  "id": "c5163",
                  "iabid": 0,
                  "systemid": "c5163",
                  "googleid": 0,
                  "name": "Firebase Crashlytics"
                },
                {
                  "id": 78,
                  "iabid": 78,
                  "systemid": "s129",
                  "googleid": 209,
                  "name": "Flashtalking, Inc."
                },
                {
                  "id": 328,
                  "iabid": 328,
                  "systemid": "s133",
                  "googleid": 221,
                  "name": "Gemius SA"
                },
                {
                  "id": 845,
                  "iabid": 845,
                  "systemid": "s1552",
                  "googleid": 0,
                  "name": "GeoEdge"
                },
                {
                  "id": 758,
                  "iabid": 758,
                  "systemid": "s1444",
                  "googleid": 0,
                  "name": "GfK SE"
                },
                {
                  "id": 536,
                  "iabid": 536,
                  "systemid": "s406",
                  "googleid": 1705,
                  "name": "GlobalWebIndex"
                },
                {
                  "id": 967,
                  "iabid": 967,
                  "systemid": "s2429",
                  "googleid": 0,
                  "name": "glomex GmbH"
                },
                {
                  "id": 755,
                  "iabid": 755,
                  "systemid": "s1498",
                  "googleid": 0,
                  "name": "Google Advertising Products"
                },
                {
                  "id": "s26",
                  "iabid": 0,
                  "systemid": "s26",
                  "googleid": 0,
                  "name": "Google Analytics"
                },
                {
                  "id": "s1104",
                  "iabid": 0,
                  "systemid": "s1104",
                  "googleid": 0,
                  "name": "Google Maps"
                },
                {
                  "id": 657,
                  "iabid": 657,
                  "systemid": "s664",
                  "googleid": 2586,
                  "name": "GP One GmbH"
                },
                {
                  "id": 98,
                  "iabid": 98,
                  "systemid": "s742",
                  "googleid": 2779,
                  "name": "GroupM UK Limited"
                },
                {
                  "id": 131,
                  "iabid": 131,
                  "systemid": "s1376",
                  "googleid": 0,
                  "name": "ID5 Technology Ltd"
                },
                {
                  "id": 921,
                  "iabid": 921,
                  "systemid": "s1637",
                  "googleid": 0,
                  "name": "Imonomy"
                },
                {
                  "id": 606,
                  "iabid": 606,
                  "systemid": "s414",
                  "googleid": 1735,
                  "name": "Impactify "
                },
                {
                  "id": 253,
                  "iabid": 253,
                  "systemid": "s721",
                  "googleid": 2705,
                  "name": "Improve Digital"
                },
                {
                  "id": 10,
                  "iabid": 10,
                  "systemid": "s105",
                  "googleid": 126,
                  "name": "Index Exchange, Inc. "
                },
                {
                  "id": 730,
                  "iabid": 730,
                  "systemid": "s1481",
                  "googleid": 0,
                  "name": "INFOnline GmbH"
                },
                {
                  "id": 333,
                  "iabid": 333,
                  "systemid": "s302",
                  "googleid": 1172,
                  "name": "InMobi Pte Ltd"
                },
                {
                  "id": 452,
                  "iabid": 452,
                  "systemid": "s219",
                  "googleid": 588,
                  "name": "Innovid Inc."
                },
                {
                  "id": 150,
                  "iabid": 150,
                  "systemid": "s1379",
                  "googleid": 3001,
                  "name": "Inskin Media LTD"
                },
                {
                  "id": "s14",
                  "iabid": 0,
                  "systemid": "s14",
                  "googleid": 0,
                  "name": "Instagram"
                },
                {
                  "id": 278,
                  "iabid": 278,
                  "systemid": "s88",
                  "googleid": 48,
                  "name": "Integral Ad Science, Inc."
                },
                {
                  "id": 436,
                  "iabid": 436,
                  "systemid": "s1116",
                  "googleid": 0,
                  "name": "INVIBES GROUP"
                },
                {
                  "id": 129,
                  "iabid": 129,
                  "systemid": "s143",
                  "googleid": 271,
                  "name": "IPONWEB GmbH"
                },
                {
                  "id": 252,
                  "iabid": 252,
                  "systemid": "s1146",
                  "googleid": 0,
                  "name": "Jaduda GmbH"
                },
                {
                  "id": 294,
                  "iabid": 294,
                  "systemid": "s145",
                  "googleid": 274,
                  "name": "Jivox Corporation"
                },
                {
                  "id": 62,
                  "iabid": 62,
                  "systemid": "s757",
                  "googleid": 2808,
                  "name": "Justpremium BV"
                },
                {
                  "id": 747,
                  "iabid": 747,
                  "systemid": "s1414",
                  "googleid": 0,
                  "name": "Kairion GmbH"
                },
                {
                  "id": 528,
                  "iabid": 528,
                  "systemid": "s399",
                  "googleid": 1674,
                  "name": "Kayzen"
                },
                {
                  "id": "c12756",
                  "iabid": 0,
                  "systemid": "c12756",
                  "googleid": 0,
                  "name": "kicker-Trendmonitor (Mindline) "
                },
                {
                  "id": "c4499",
                  "iabid": 0,
                  "systemid": "c4499",
                  "googleid": 0,
                  "name": "kicker.de"
                },
                {
                  "id": 424,
                  "iabid": 424,
                  "systemid": "s369",
                  "googleid": 1542,
                  "name": "KUPONA GmbH"
                },
                {
                  "id": 804,
                  "iabid": 804,
                  "systemid": "s1513",
                  "googleid": 0,
                  "name": "LinkedIn Ireland Unlimited Company"
                },
                {
                  "id": 254,
                  "iabid": 254,
                  "systemid": "s1392",
                  "googleid": 986,
                  "name": "LiquidM Technology GmbH"
                },
                {
                  "id": 97,
                  "iabid": 97,
                  "systemid": "s80",
                  "googleid": 12,
                  "name": "LiveRamp, Inc."
                },
                {
                  "id": 95,
                  "iabid": 95,
                  "systemid": "s148",
                  "googleid": 294,
                  "name": "Lotame Solutions, inc"
                },
                {
                  "id": 508,
                  "iabid": 508,
                  "systemid": "s461",
                  "googleid": 1905,
                  "name": "Lucid Holdings, LLC"
                },
                {
                  "id": 764,
                  "iabid": 764,
                  "systemid": "s1454",
                  "googleid": 0,
                  "name": "Lucidity"
                },
                {
                  "id": 846,
                  "iabid": 846,
                  "systemid": "s1553",
                  "googleid": 0,
                  "name": "M,P,NEWMEDIA, GmbH"
                },
                {
                  "id": 52,
                  "iabid": 52,
                  "systemid": "s270",
                  "googleid": 1029,
                  "name": "Magnite, Inc. "
                },
                {
                  "id": "c5136",
                  "iabid": 0,
                  "systemid": "c5136",
                  "googleid": 0,
                  "name": "MDR"
                },
                {
                  "id": 498,
                  "iabid": 498,
                  "systemid": "s1058",
                  "googleid": 0,
                  "name": "Mediakeys Platform"
                },
                {
                  "id": 79,
                  "iabid": 79,
                  "systemid": "s153",
                  "googleid": 313,
                  "name": "MediaMath, Inc."
                },
                {
                  "id": 152,
                  "iabid": 152,
                  "systemid": "s220",
                  "googleid": 590,
                  "name": "Meetrics GmbH"
                },
                {
                  "id": 703,
                  "iabid": 703,
                  "systemid": "s1041",
                  "googleid": 0,
                  "name": "MindTake Research GmbH"
                },
                {
                  "id": 101,
                  "iabid": 101,
                  "systemid": "s151",
                  "googleid": 310,
                  "name": "MiQ"
                },
                {
                  "id": 311,
                  "iabid": 311,
                  "systemid": "s149",
                  "googleid": 303,
                  "name": "Mobfox US LLC"
                },
                {
                  "id": 302,
                  "iabid": 302,
                  "systemid": "s1069",
                  "googleid": 0,
                  "name": "Mobile Professionals BV"
                },
                {
                  "id": 898,
                  "iabid": 898,
                  "systemid": "s1610",
                  "googleid": 0,
                  "name": "Mobkoi Ltd"
                },
                {
                  "id": 737,
                  "iabid": 737,
                  "systemid": "s1124",
                  "googleid": 0,
                  "name": "Monet Engine Inc"
                },
                {
                  "id": 72,
                  "iabid": 72,
                  "systemid": "s338",
                  "googleid": 1344,
                  "name": "Nano Interactive GmbH"
                },
                {
                  "id": 34,
                  "iabid": 34,
                  "systemid": "s619",
                  "googleid": 2464,
                  "name": "NEORY GmbH"
                },
                {
                  "id": 521,
                  "iabid": 521,
                  "systemid": "s1365",
                  "googleid": 0,
                  "name": "netzeffekt GmbH"
                },
                {
                  "id": 468,
                  "iabid": 468,
                  "systemid": "s1402",
                  "googleid": 0,
                  "name": "NeuStar, Inc."
                },
                {
                  "id": "c6925",
                  "iabid": 0,
                  "systemid": "c6925",
                  "googleid": 0,
                  "name": "New Relic"
                },
                {
                  "id": 502,
                  "iabid": 502,
                  "systemid": "s549",
                  "googleid": 2247,
                  "name": "NEXD"
                },
                {
                  "id": 130,
                  "iabid": 130,
                  "systemid": "s920",
                  "googleid": 432,
                  "name": "NextRoll, Inc."
                },
                {
                  "id": 812,
                  "iabid": 812,
                  "systemid": "s1600",
                  "googleid": 0,
                  "name": "Nielsen International SA"
                },
                {
                  "id": 373,
                  "iabid": 373,
                  "systemid": "s163",
                  "googleid": 348,
                  "name": "Nielsen Marketing Cloud"
                },
                {
                  "id": 832,
                  "iabid": 832,
                  "systemid": "s1538",
                  "googleid": 0,
                  "name": "Objective Partners BV"
                },
                {
                  "id": 31,
                  "iabid": 31,
                  "systemid": "s720",
                  "googleid": 2704,
                  "name": "Ogury Ltd."
                },
                {
                  "id": 509,
                  "iabid": 509,
                  "systemid": "s1247",
                  "googleid": 0,
                  "name": "One Tech Group GmbH"
                },
                {
                  "id": 617,
                  "iabid": 617,
                  "systemid": "s1361",
                  "googleid": 0,
                  "name": "Onfocus (Adagio)"
                },
                {
                  "id": 883,
                  "iabid": 883,
                  "systemid": "s1589",
                  "googleid": 0,
                  "name": "Online Media Solutions LTD (BDA: Brightcom)"
                },
                {
                  "id": 602,
                  "iabid": 602,
                  "systemid": "s1179",
                  "googleid": 0,
                  "name": "Online Solution"
                },
                {
                  "id": 1049,
                  "iabid": 1049,
                  "systemid": "s2541",
                  "googleid": 0,
                  "name": "OnProspects Ltd"
                },
                {
                  "id": 280,
                  "iabid": 280,
                  "systemid": "s890",
                  "googleid": 0,
                  "name": "OpenWeb LTD"
                },
                {
                  "id": 69,
                  "iabid": 69,
                  "systemid": "s167",
                  "googleid": 363,
                  "name": "OpenX"
                },
                {
                  "id": 488,
                  "iabid": 488,
                  "systemid": "s1359",
                  "googleid": 0,
                  "name": "Opinary GmbH"
                },
                {
                  "id": 349,
                  "iabid": 349,
                  "systemid": "s562",
                  "googleid": 2299,
                  "name": "Optomaton UG"
                },
                {
                  "id": 385,
                  "iabid": 385,
                  "systemid": "s741",
                  "googleid": 2778,
                  "name": "Oracle Data Cloud"
                },
                {
                  "id": 772,
                  "iabid": 772,
                  "systemid": "s1467",
                  "googleid": 0,
                  "name": "Oracle Data Cloud - Moat"
                },
                {
                  "id": 559,
                  "iabid": 559,
                  "systemid": "s503",
                  "googleid": 2079,
                  "name": "Otto (GmbH &amp; Co KG)"
                },
                {
                  "id": 164,
                  "iabid": 164,
                  "systemid": "s1141",
                  "googleid": 0,
                  "name": "Outbrain UK Ltd"
                },
                {
                  "id": 139,
                  "iabid": 139,
                  "systemid": "s456",
                  "googleid": 1892,
                  "name": "Permodo GmbH"
                },
                {
                  "id": 384,
                  "iabid": 384,
                  "systemid": "s301",
                  "googleid": 1171,
                  "name": "Pixalate, Inc."
                },
                {
                  "id": 140,
                  "iabid": 140,
                  "systemid": "s107",
                  "googleid": 134,
                  "name": "Platform161 B.V."
                },
                {
                  "id": 1070,
                  "iabid": 1070,
                  "systemid": "s2567",
                  "googleid": 0,
                  "name": "Playhill Limited"
                },
                {
                  "id": 177,
                  "iabid": 177,
                  "systemid": "s1085",
                  "googleid": 0,
                  "name": "plista GmbH"
                },
                {
                  "id": 297,
                  "iabid": 297,
                  "systemid": "s1348",
                  "googleid": 3015,
                  "name": "Polar Mobile Group Inc."
                },
                {
                  "id": 226,
                  "iabid": 226,
                  "systemid": "s743",
                  "googleid": 2780,
                  "name": "Publicis Media GmbH"
                },
                {
                  "id": 76,
                  "iabid": 76,
                  "systemid": "s269",
                  "googleid": 1028,
                  "name": "PubMatic, Inc."
                },
                {
                  "id": 81,
                  "iabid": 81,
                  "systemid": "s118",
                  "googleid": 162,
                  "name": "PulsePoint, Inc."
                },
                {
                  "id": 808,
                  "iabid": 808,
                  "systemid": "s1515",
                  "googleid": 3206,
                  "name": "Pure Local Media GmbH"
                },
                {
                  "id": "c7758",
                  "iabid": 0,
                  "systemid": "c7758",
                  "googleid": 0,
                  "name": "Qualifio"
                },
                {
                  "id": 835,
                  "iabid": 835,
                  "systemid": "s1541",
                  "googleid": 0,
                  "name": "Quality Media Network GmbH"
                },
                {
                  "id": 11,
                  "iabid": 11,
                  "systemid": "s177",
                  "googleid": 398,
                  "name": "Quantcast International Limited"
                },
                {
                  "id": 993,
                  "iabid": 993,
                  "systemid": "s2469",
                  "googleid": 0,
                  "name": "QUARTER MEDIA GmbH"
                },
                {
                  "id": 60,
                  "iabid": 60,
                  "systemid": "s232",
                  "googleid": 782,
                  "name": "Rakuten Marketing LLC"
                },
                {
                  "id": 800,
                  "iabid": 800,
                  "systemid": "s1510",
                  "googleid": 0,
                  "name": "Reppublika- The Research Toolbox GmbH"
                },
                {
                  "id": 787,
                  "iabid": 787,
                  "systemid": "s1516",
                  "googleid": 0,
                  "name": "Resolution Media München GmbH"
                },
                {
                  "id": 863,
                  "iabid": 863,
                  "systemid": "s1570",
                  "googleid": 0,
                  "name": "respondi AG"
                },
                {
                  "id": 108,
                  "iabid": 108,
                  "systemid": "s1331",
                  "googleid": 0,
                  "name": "Rich Audience Technologies SL"
                },
                {
                  "id": 71,
                  "iabid": 71,
                  "systemid": "s117",
                  "googleid": 161,
                  "name": "Roku Advertising Services"
                },
                {
                  "id": 4,
                  "iabid": 4,
                  "systemid": "s527",
                  "googleid": 2170,
                  "name": "Roq.ad Inc."
                },
                {
                  "id": 16,
                  "iabid": 16,
                  "systemid": "s366",
                  "googleid": 1520,
                  "name": "RTB House S.A."
                },
                {
                  "id": 506,
                  "iabid": 506,
                  "systemid": "s291",
                  "googleid": 1126,
                  "name": "Salesforce.com, Inc."
                },
                {
                  "id": 371,
                  "iabid": 371,
                  "systemid": "s1016",
                  "googleid": 0,
                  "name": "Seeding Alliance GmbH"
                },
                {
                  "id": 157,
                  "iabid": 157,
                  "systemid": "s759",
                  "googleid": 2810,
                  "name": "Seedtag Advertising S.L"
                },
                {
                  "id": 84,
                  "iabid": 84,
                  "systemid": "s259",
                  "googleid": 976,
                  "name": "Semasio GmbH"
                },
                {
                  "id": 111,
                  "iabid": 111,
                  "systemid": "s1318",
                  "googleid": 3181,
                  "name": "Showheroes SE"
                },
                {
                  "id": 276,
                  "iabid": 276,
                  "systemid": "s1202",
                  "googleid": 0,
                  "name": "ShowHeroes SRL"
                },
                {
                  "id": 261,
                  "iabid": 261,
                  "systemid": "s349",
                  "googleid": 1419,
                  "name": "Signal Digital Inc."
                },
                {
                  "id": 68,
                  "iabid": 68,
                  "systemid": "s154",
                  "googleid": 314,
                  "name": "Sizmek by Amazon"
                },
                {
                  "id": 82,
                  "iabid": 82,
                  "systemid": "s724",
                  "googleid": 2709,
                  "name": "Smaato, Inc."
                },
                {
                  "id": 161,
                  "iabid": 161,
                  "systemid": "s392",
                  "googleid": 1652,
                  "name": "Smadex SLU"
                },
                {
                  "id": 45,
                  "iabid": 45,
                  "systemid": "s1156",
                  "googleid": 0,
                  "name": "Smart Adserver"
                },
                {
                  "id": 115,
                  "iabid": 115,
                  "systemid": "s189",
                  "googleid": 443,
                  "name": "smartclip Europe GmbH"
                },
                {
                  "id": 134,
                  "iabid": 134,
                  "systemid": "s283",
                  "googleid": 1072,
                  "name": "SMARTSTREAM.TV GmbH"
                },
                {
                  "id": 966,
                  "iabid": 966,
                  "systemid": "s2428",
                  "googleid": 0,
                  "name": "socoto gmbh & co. kg"
                },
                {
                  "id": 937,
                  "iabid": 937,
                  "systemid": "s1665",
                  "googleid": 0,
                  "name": "SoD ScreenOnDemand GmbH"
                },
                {
                  "id": 104,
                  "iabid": 104,
                  "systemid": "s840",
                  "googleid": 2928,
                  "name": "Sonobi, Inc"
                },
                {
                  "id": 13,
                  "iabid": 13,
                  "systemid": "s722",
                  "googleid": 2706,
                  "name": "Sovrn Holdings Inc"
                },
                {
                  "id": "c12084",
                  "iabid": 0,
                  "systemid": "c12084",
                  "googleid": 0,
                  "name": "SpeechKit"
                },
                {
                  "id": 951,
                  "iabid": 951,
                  "systemid": "s2411",
                  "googleid": 0,
                  "name": "Spoods GmbH"
                },
                {
                  "id": 655,
                  "iabid": 655,
                  "systemid": "s1309",
                  "googleid": 3020,
                  "name": "Sportradar AG"
                },
                {
                  "id": 165,
                  "iabid": 165,
                  "systemid": "s193",
                  "googleid": 459,
                  "name": "SpotX, Inc"
                },
                {
                  "id": 238,
                  "iabid": 238,
                  "systemid": "s430",
                  "googleid": 1801,
                  "name": "StackAdapt"
                },
                {
                  "id": "c5334",
                  "iabid": 0,
                  "systemid": "c5334",
                  "googleid": 0,
                  "name": "Stats Perform Group"
                },
                {
                  "id": 950,
                  "iabid": 950,
                  "systemid": "s2410",
                  "googleid": 0,
                  "name": "Stream Eye OOD"
                },
                {
                  "id": 1057,
                  "iabid": 1057,
                  "systemid": "s2552",
                  "googleid": 0,
                  "name": "Ströer Digital Media GmbH"
                },
                {
                  "id": 137,
                  "iabid": 137,
                  "systemid": "s334",
                  "googleid": 1313,
                  "name": "Ströer SSP GmbH (DSP)"
                },
                {
                  "id": 136,
                  "iabid": 136,
                  "systemid": "s1140",
                  "googleid": 0,
                  "name": "Ströer SSP GmbH (SSP)"
                },
                {
                  "id": 114,
                  "iabid": 114,
                  "systemid": "s754",
                  "googleid": 2802,
                  "name": "Sublime"
                },
                {
                  "id": 484,
                  "iabid": 484,
                  "systemid": "s923",
                  "googleid": 624,
                  "name": "SublimeSkinz - Adledge"
                },
                {
                  "id": 275,
                  "iabid": 275,
                  "systemid": "s580",
                  "googleid": 2346,
                  "name": "TabMo SAS"
                },
                {
                  "id": 42,
                  "iabid": 42,
                  "systemid": "s98",
                  "googleid": 86,
                  "name": "Taboola Europe Limited"
                },
                {
                  "id": 89,
                  "iabid": 89,
                  "systemid": "s1068",
                  "googleid": 469,
                  "name": "Tapad, Inc."
                },
                {
                  "id": 475,
                  "iabid": 475,
                  "systemid": "s450",
                  "googleid": 1875,
                  "name": "TAPTAP Digital SL"
                },
                {
                  "id": 786,
                  "iabid": 786,
                  "systemid": "s1488",
                  "googleid": 0,
                  "name": "TargetVideo GmbH"
                },
                {
                  "id": 132,
                  "iabid": 132,
                  "systemid": "s18",
                  "googleid": 1684,
                  "name": "Teads "
                },
                {
                  "id": 522,
                  "iabid": 522,
                  "systemid": "s462",
                  "googleid": 1911,
                  "name": "Tealium Inc."
                },
                {
                  "id": 44,
                  "iabid": 44,
                  "systemid": "s407",
                  "googleid": 1716,
                  "name": "The ADEX GmbH"
                },
                {
                  "id": 345,
                  "iabid": 345,
                  "systemid": "s319",
                  "googleid": 1236,
                  "name": "The Kantar Group Limited"
                },
                {
                  "id": 382,
                  "iabid": 382,
                  "systemid": "s238",
                  "googleid": 814,
                  "name": "The Reach Group GmbH"
                },
                {
                  "id": 21,
                  "iabid": 21,
                  "systemid": "s194",
                  "googleid": 479,
                  "name": "The Trade Desk"
                },
                {
                  "id": "c5158",
                  "iabid": 0,
                  "systemid": "c5158",
                  "googleid": 0,
                  "name": "Tickaroo Live.Blog"
                },
                {
                  "id": 423,
                  "iabid": 423,
                  "systemid": "s320",
                  "googleid": 1241,
                  "name": "travel audience GmbH"
                },
                {
                  "id": 28,
                  "iabid": 28,
                  "systemid": "s255",
                  "googleid": 933,
                  "name": "TripleLift, Inc."
                },
                {
                  "id": 242,
                  "iabid": 242,
                  "systemid": "s470",
                  "googleid": 1945,
                  "name": "twiago GmbH"
                },
                {
                  "id": "c5135",
                  "iabid": 0,
                  "systemid": "c5135",
                  "googleid": 0,
                  "name": "Twitch"
                },
                {
                  "id": "s34",
                  "iabid": 0,
                  "systemid": "s34",
                  "googleid": 0,
                  "name": "Twitter"
                },
                {
                  "id": 828,
                  "iabid": 828,
                  "systemid": "s1534",
                  "googleid": 0,
                  "name": "TX Group AG"
                },
                {
                  "id": 162,
                  "iabid": 162,
                  "systemid": "s203",
                  "googleid": 510,
                  "name": "Unruly Group Ltd"
                },
                {
                  "id": 459,
                  "iabid": 459,
                  "systemid": "s1210",
                  "googleid": 2899,
                  "name": "uppr GmbH"
                },
                {
                  "id": 512,
                  "iabid": 512,
                  "systemid": "s1024",
                  "googleid": 2977,
                  "name": "Verve Group Europe GmbH"
                },
                {
                  "id": 237,
                  "iabid": 237,
                  "systemid": "s601",
                  "googleid": 2411,
                  "name": "VGI CTV, Inc"
                },
                {
                  "id": 1036,
                  "iabid": 1036,
                  "systemid": "s2526",
                  "googleid": 0,
                  "name": "VIADS ADVERTISING S.L."
                },
                {
                  "id": 190,
                  "iabid": 190,
                  "systemid": "s1206",
                  "googleid": 0,
                  "name": "video intelligence AG"
                },
                {
                  "id": 1009,
                  "iabid": 1009,
                  "systemid": "s2496",
                  "googleid": 0,
                  "name": "VLYBY Digital GmbH"
                },
                {
                  "id": 774,
                  "iabid": 774,
                  "systemid": "s1477",
                  "googleid": 3107,
                  "name": "Wagawin GmbH"
                },
                {
                  "id": 284,
                  "iabid": 284,
                  "systemid": "s206",
                  "googleid": 528,
                  "name": "Weborama"
                },
                {
                  "id": 282,
                  "iabid": 282,
                  "systemid": "s1198",
                  "googleid": 0,
                  "name": "Welect GmbH"
                },
                {
                  "id": 281,
                  "iabid": 281,
                  "systemid": "s449",
                  "googleid": 1872,
                  "name": "Wizaly"
                },
                {
                  "id": 32,
                  "iabid": 32,
                  "systemid": "s915",
                  "googleid": 80,
                  "name": "Xandr, Inc."
                },
                {
                  "id": 25,
                  "iabid": 25,
                  "systemid": "s740",
                  "googleid": 2777,
                  "name": "Yahoo EMEA Limited"
                },
                {
                  "id": 70,
                  "iabid": 70,
                  "systemid": "s1194",
                  "googleid": 0,
                  "name": "Yieldlab AG"
                },
                {
                  "id": 251,
                  "iabid": 251,
                  "systemid": "s1193",
                  "googleid": 0,
                  "name": "Yieldlove GmbH"
                },
                {
                  "id": 154,
                  "iabid": 154,
                  "systemid": "s1192",
                  "googleid": 1178,
                  "name": "YOC AG"
                },
                {
                  "id": 726,
                  "iabid": 726,
                  "systemid": "s607",
                  "googleid": 2422,
                  "name": "YouGov"
                },
                {
                  "id": "s30",
                  "iabid": 0,
                  "systemid": "s30",
                  "googleid": 0,
                  "name": "YouTube"
                },
                {
                  "id": 210,
                  "iabid": 210,
                  "systemid": "s1168",
                  "googleid": 0,
                  "name": "Zemanta, Inc."
                },
                {
                  "id": 301,
                  "iabid": 301,
                  "systemid": "s770",
                  "googleid": 2829,
                  "name": "zeotap GmbH"
                }
              ],
              "publisherCC": "eu",
              "addtlConsent": "1~587.89",
              "purposesList": [
                {
                  "id": "c1"
                },
                {
                  "id": "1"
                },
                {
                  "id": "2"
                },
                {
                  "id": "3"
                },
                {
                  "id": "4"
                },
                {
                  "id": "5"
                },
                {
                  "id": "6"
                },
                {
                  "id": "7"
                },
                {
                  "id": "8"
                },
                {
                  "id": "9"
                },
                {
                  "id": "10"
                },
                {
                  "id": "s1"
                },
                {
                  "id": "s2"
                },
                {
                  "id": "r1"
                },
                {
                  "id": "r2"
                },
                {
                  "id": "c19"
                }
              ]
            }
        """.trimIndent()
}