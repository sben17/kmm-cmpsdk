package net.consentmanager.cmpsdk.infrastructure

import com.russhwolf.settings.MockSettings
import kotlinx.datetime.Instant
import net.consentmanager.cmpsdk.model.CmpState
import kotlin.test.Test
import kotlin.test.assertEquals

class CmpStatusRepositoryTests {

    private val cmpStatusRepository: CmpStatusRepository = CmpStatusRepository(MockSettings())

    @Test
    fun testSaveCmpState() {
     cmpStatusRepository.saveState(CmpState.initial())
    }

    @Test
    fun testGetCmpState() {
        val cmpState : CmpState = CmpState.initial()
        cmpStatusRepository.saveState(cmpState)
        val savedState : CmpState? = cmpStatusRepository.getState()
        assertEquals(cmpState.state, savedState!!.state)
        assertEquals(cmpState.timeStamp, savedState.timeStamp)
        assertEquals(cmpState.layerState, savedState.layerState)
        assertEquals(cmpState.message, savedState.message)
    }

    @Test
    fun testSaveLastConsentGiven() {
        cmpStatusRepository.saveLastConsentGiven()

    }

    @Test
    fun testGetLastConsentGiven() {
        cmpStatusRepository.saveLastConsentGiven()
        val instant : Instant = cmpStatusRepository.getLastConsentGiven()
        print(instant.toString())
    }

}