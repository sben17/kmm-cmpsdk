package net.consentmanager.cmpsdk.service

import com.russhwolf.settings.MockSettings
import net.consentmanager.cmpsdk.assets.getExampleJson
import net.consentmanager.cmpsdk.infrastructure.CmpStatusRepository
import net.consentmanager.cmpsdk.infrastructure.CmpUserConsentRepository
import net.consentmanager.cmpsdk.model.CmpStateName
import kotlin.test.BeforeTest
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class CmpUserConsentServiceTests {

    private val cmpUserConsentRepository : CmpUserConsentRepository = CmpUserConsentRepository(
        MockSettings()
    )

    private val cmpStatusRepository : CmpStatusRepository = CmpStatusRepository(MockSettings())

    @BeforeTest
    fun setUp() {
        cmpUserConsentRepository.saveUserConsent(getExampleJson())
    }

    @Test
    fun testHasVendor() {
        val cmpService : CmpUserConsentService = CmpUserConsentService(cmpUserConsentRepository, cmpStatusRepository)
        assertTrue(cmpService.hasVendor("4"))
        assertTrue(cmpService.hasVendor("4"))
        assertTrue(cmpService.hasVendor("4"))
        assertTrue(cmpService.hasVendor("4"))
    }

    @Test
    fun testHasPurpose() {
        val cmpService : CmpUserConsentService = CmpUserConsentService(cmpUserConsentRepository, cmpStatusRepository)
        assertTrue(cmpService.hasPurpose("4"))
        assertTrue(cmpService.hasPurpose("4"))
        assertTrue(cmpService.hasPurpose("4"))
        assertTrue(cmpService.hasPurpose("4"))
    }

    @Test
    fun testResetUserConsent() {
        val cmpService : CmpUserConsentService = CmpUserConsentService(cmpUserConsentRepository, cmpStatusRepository)
        cmpService.resetUserConsent()
        // after reset user consent, also Status should be resettet
        assertTrue(cmpService.getCmpId() == "")
        assertEquals(CmpStateName.INITIAL, cmpService.getState().state)
    }

    fun testGetCmpId() {

    }

    fun testGetUserConsent() {

    }

    fun testGetVendors() {

    }

    fun testGetPurposeList() {

    }
}