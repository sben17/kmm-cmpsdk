package net.consentmanager.cmpsdk.utils

import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFalse
import kotlin.test.assertTrue

class CmpUtilsTests {

    @Test
    fun testIsValidPurpose() {
        assertTrue(isValidPurpose("4"))
        assertTrue(isValidPurpose("c4"))
        assertTrue(isValidPurpose("s4"))

        assertFalse(isValidPurpose("-4"))
        assertFalse(isValidPurpose("z1"))
        assertFalse(isValidPurpose(""))
    }
    @Test
    fun testGetVendorTypes() {
        assertEquals(getVendorType("c3"), VendorTypeEnum.CUSTOM_VENDOR)
        assertEquals(getVendorType("s3912"), VendorTypeEnum.SYSTEM_VENDOR)
        assertEquals(getVendorType("3"), VendorTypeEnum.IAB_VENDOR)

        assertEquals(getVendorType("c3"), VendorTypeEnum.CUSTOM_VENDOR)
        assertEquals(getVendorType("s31231"), VendorTypeEnum.SYSTEM_VENDOR)
        assertEquals(getVendorType("123"), VendorTypeEnum.IAB_VENDOR)
    }

    fun expectException() {

    }

    @Test
    fun testIsValidVendor() {
        assertTrue(isValidVendor("4"))
        assertTrue(isValidVendor("c4"))
        assertTrue(isValidVendor("s4"))

        assertFalse(isValidVendor("-4"))
        assertFalse(isValidVendor("z1"))
        assertFalse(isValidVendor(""))
    }
    @Test
    fun testGetPurposeType() {
        assertEquals(getPurposeType("c3"), PurposeTypeEnum.CUSTOM_PURPOSE)
        assertEquals(getPurposeType("s3912"), PurposeTypeEnum.SYSTEM_PURPOSE)
        assertEquals(getPurposeType("3"), PurposeTypeEnum.IAB_PURPOSE)

        assertEquals(getPurposeType("c3"), PurposeTypeEnum.CUSTOM_PURPOSE)
        assertEquals(getPurposeType("s31231"), PurposeTypeEnum.SYSTEM_PURPOSE)
        assertEquals(getPurposeType("123"), PurposeTypeEnum.IAB_PURPOSE)
    }
}
