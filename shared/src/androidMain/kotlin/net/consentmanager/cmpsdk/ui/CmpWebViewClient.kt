package net.consentmanager.cmpsdk.ui

import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.webkit.WebResourceRequest
import android.webkit.WebView
import android.webkit.WebViewClient

private const val CMP_URL_PREFIX : String = "consent://"
class CmpWebViewClient(
) : WebViewClient() {

    override fun onPageStarted(view: WebView?, url: String?, favicon: Bitmap?) {
        super.onPageStarted(view, url, favicon)
    }

    override fun shouldOverrideUrlLoading(view: WebView?, request: WebResourceRequest?): Boolean {
        if (request != null) {
            // check if consent:// is called
            if(request.url.toString().contains(CMP_URL_PREFIX)) {
                // extra steps to exit screen
                return true;
            }
            // TODO start browser intent
            return true
        }
        // Default if not consent://
        return false;
    }

    override fun onLoadResource(view: WebView?, url: String?) {
        val sendStatus : String =  "javascript:window.cmpToSDK_sendStatus = function(consent, jsonObject) { jsonObject.cmpId = consent; Android.onReceivedConsent(consent, JSON.stringify(jsonObject)); };";
        val openScreen : String ="javascript:window.cmpToSDK_showConsentLayer = function() { Android.onOpenCmpLayer();};";
        view?.loadUrl(openScreen)
        view?.loadUrl(sendStatus)
    }
}