package net.consentmanager.cmpsdk.ui

import android.util.Log
import android.view.View
import android.webkit.JavascriptInterface
import net.consentmanager.cmpsdk.infrastructure.CmpUserConsentRepository

private const val TAG : String = "CMP:Js"

class CmpConsentAppInterface(
    private val mView: View? = null,
    private val consentRepository: CmpUserConsentRepository,
) {

    @JavascriptInterface
    fun onOpen() {
        Log.d(TAG, "Cmp Screen opened")
        // TODO open WebView Fragment
    }

    @JavascriptInterface
    fun onUserConsent(cmpJson: String) {
        Log.d(TAG, "User consent saved")
        consentRepository.saveUserConsent(cmpJson)
    }
}